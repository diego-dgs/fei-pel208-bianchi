# Install Dependencies

```
$ sudo apt install build-essential cmake gnuplot libeigen3-dev
```

# Build

```
$ cd fei-pel208-bianchi/entrega05/
$ mkdir build
$ cd build
$ cmake .. 
$ make -j
```

# Run

```
$ cd fei-pel208-bianchi/entrega05/bin
$ ./perceptron
```