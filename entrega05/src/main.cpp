#include "matrix2d.h"
#include "least_squares.h"
#include "datasets.h"
#include "lda.h"
#include "pca.h"
#include "kmeans.h"


#include <string.h>
#include <malloc.h>
#include "vecnf.h"
#include "datasets.h"
#include "layers.h"
#include "activations.h"
#include "optimizers.h"


void unit_tests();

int main(int argc, char *argv[])
{
    // std::cout << "Learning AND" << std::endl;
    // {// Learning AND
    //     std::pair< std::vector<VecNf>, std::vector<VecNf> > data;
    //     data.first.push_back(VecNf::from_vector({1.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({1.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({1.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({1.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,1.f}));data.second.push_back(VecNf::from_vector({0.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 


    //     PlaceHolder a(data.first[0].size());

    //     Dense d(1, sigmoid, a);
        
    //     Sequential model(d);
        
    //     SGD(&model, data.first, data.second, 1000, 3, 0.1f);


    //     // plot AND results
    //     model.setBatchSize(1);

    //     std::ofstream myfile[2];

    //     std::remove("/tmp/perceptron0");
    //     std::remove("/tmp/perceptron1");

    //     myfile[0].open("/tmp/perceptron0");
    //     myfile[1].open("/tmp/perceptron1");
 
    //     for(size_t i = 0; i < data.first.size(); i++){
    //         VecNf X = data.first[i];
    //         std::vector<VecNf> in = {X};
    //         std::vector<VecNf> * out;
            

    //         model.fast_forward(in, out);

    //         bool result = (*out)[0][0] > 0.5f;

    //         myfile[result] << X[0] << "\t" << X[1] << "\n";

            
    //     }
    //     myfile[0].close();
    //     myfile[1].close();

    //     GnuplotPipe gp;

    //     std::stringstream ss;

    //     ss << "set xrange [" << -1 << ": " << 2  << "];";
    //     ss << "set yrange [" << -1 << ": " << 2  << "];";
    //     std::string title = "AND";
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";
    //     ss << "plot";
    //     for (size_t i = 0; i < 2; i++)
    //     {
    //         std::stringstream fname;
    //         fname << "/tmp/perceptron" << i ;
    //         ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
    //         ss << " title \"ID: " << i << "\"";
            
    //         // if (i < (2 - 1))
    //             ss << ", ";
    //     }
    //     auto W = d.getWeights(0);
    //     ss << " " << -W[2]/W[1] << " " << std::showpos << (-(W[2]/W[1]) / (W[2]/W[0]) )  << "*x title \"AND Decision Boundary\"; ";
    //     std::cout << ss.str() << "\n";
    //     gp.sendLine(ss.str());

    // }
    // std::cout << "Learning OR" << std::endl;
    // {// Learning OR
    //     std::pair< std::vector<VecNf>, std::vector<VecNf> > data;

    //     data.first.push_back(VecNf::from_vector({1.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({1.f,0.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 
    //     data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 


    //     PlaceHolder a(data.first[0].size());

    //     Dense d(1, sigmoid, a);
        
    //     Sequential model(d);
        
    //     SGD(&model, data.first, data.second, 1000, 3, 1e-1);


    //     // plot OR results
    //     model.setBatchSize(1);

    //     std::ofstream myfile[2];

    //     std::remove("/tmp/perceptron0");
    //     std::remove("/tmp/perceptron1");

    //     myfile[0].open("/tmp/perceptron0");
    //     myfile[1].open("/tmp/perceptron1");
 
    //     for(size_t i = 0; i < data.first.size(); i++){
    //         VecNf X = data.first[i];
    //         std::vector<VecNf> in = {X};
    //         std::vector<VecNf> * out;
            

    //         model.fast_forward(in, out);

    //         bool result = (*out)[0][0] > 0.5f;

    //         myfile[result] << X[0] << "\t" << X[1] << "\n";

            
    //     }
    //     myfile[0].close();
    //     myfile[1].close();

    //     GnuplotPipe gp;

    //     std::stringstream ss;

    //     ss << "set xrange [" << -1 << ": " << 2  << "];";
    //     ss << "set yrange [" << -1 << ": " << 2  << "];";
    //     std::string title = "OR";
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";
    //     ss << "plot";
    //     for (size_t i = 0; i < 2; i++)
    //     {
    //         std::stringstream fname;
    //         fname << "/tmp/perceptron" << i ;
    //         ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
    //         ss << " title \"ID: " << i << "\"";
            
    //         // if (i < (2 - 1))
    //             ss << ", ";
    //     }
    //     auto W = d.getWeights(0);
    //     ss << " " << -W[2]/W[1] << " " << std::showpos << (-(W[2]/W[1]) / (W[2]/W[0]) )  << "*x title \"OR Decision Boundary\"; ";
    //     std::cout << ss.str() << "\n";
    //     gp.sendLine(ss.str());

    // }
    std::cout << "Learning XOR" << std::endl;
    {// Learning XOR
        std::pair< std::vector<VecNf>, std::vector<VecNf> > data;

        data.first.push_back(VecNf::from_vector({1.f,1.f}));data.second.push_back(VecNf::from_vector({0.f})); 
        data.first.push_back(VecNf::from_vector({1.f,0.f}));data.second.push_back(VecNf::from_vector({1.f})); 
        data.first.push_back(VecNf::from_vector({0.f,1.f}));data.second.push_back(VecNf::from_vector({1.f})); 
        data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 
        // data.first.push_back(VecNf::from_vector({0.f,0.f}));data.second.push_back(VecNf::from_vector({0.f})); 

        PlaceHolder a(data.first[0].size());
        Dense b(2, sigmoid, a);
        Dense d(1, sigmoid, b);
        Sequential model(d);
        
        SGD(&model, data.first, data.second, 10000, 4, 0.01f);



        // plot XOR results
        model.setBatchSize(1);

        std::ofstream myfile[2];

        std::remove("/tmp/perceptron0");
        std::remove("/tmp/perceptron1");

        myfile[0].open("/tmp/perceptron0");
        myfile[1].open("/tmp/perceptron1");
 
        for(size_t i = 0; i < data.first.size(); i++){
            VecNf X = data.first[i];
            std::vector<VecNf> in = {X};
            std::vector<VecNf> * out;
            

            model.fast_forward(in, out);

            bool result = (*out)[0][0] > 0.5f;

            myfile[result] << X[0] << "\t" << X[1] << "\n";

            
        }
        myfile[0].close();
        myfile[1].close();

        GnuplotPipe gp;

        std::stringstream ss;

        ss << "set xrange [" << -1 << ": " << 2  << "];";
        ss << "set yrange [" << -1 << ": " << 2  << "];";
        std::string title = "XOR";
        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";
        ss << "plot";
        for (size_t i = 0; i < 2; i++)
        {
            std::stringstream fname;
            fname << "/tmp/perceptron" << i ;
            ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
            ss << " title \"ID: " << i << "\"";
            
            // if (i < (2 - 1))
                ss << ", ";
        }
        auto W = d.getWeights(0);
        ss << " " << -W[2]/W[1] << " " << std::showpos << (-(W[2]/W[1]) / (W[2]/W[0]) )  << "*x title \"XOR Decision Boundary\"; ";
        std::cout << ss.str() << "\n";
        gp.sendLine(ss.str());        

    }
    // std::cout << "Learning IRIS2Class" << std::endl;
    // {
    //     auto X = Datasets<float>::IRIS2Class::X();
    //     auto Y = Datasets<size_t>::IRIS2Class::Y();

    //     auto lda_data = Matrix2D<float>(X);
    //     auto lda_y = Matrix2D<size_t>(Y);

    //     auto lda = LDA<float>(lda_data, lda_y, 2, 2);

    //     auto t_data = lda.transform(lda_data);

    //     std::pair< std::vector<VecNf>, std::vector<VecNf> > data;

    //     auto y_it = Y.begin();
    //     for(size_t i =0 ; i < t_data.shape[0]; i++){
    //         VecNf y_rat = VecNf::zeros(Datasets<float>::IRIS2Class::nb_class());
    //         y_rat[(*y_it)[0]] = 1.f;
    //         data.first.push_back(VecNf::from_vector({t_data[i][0], t_data[i][1] }));
    //         data.second.push_back(y_rat);
    //         y_it++;
    //     }
        
    //     PlaceHolder a(data.first[0].size());
        
    //     Dense d(1, sigmoid, a);
        
    //     Sequential model(d);
        
    //     SGD(&model, data.first, data.second, 1000, 10,0.01f);


    // //     // plot IRIS results
    // //     model.setBatchSize(1);

    // //     std::ofstream myfile[2];

    // //     std::remove("/tmp/perceptron0");
    // //     std::remove("/tmp/perceptron1");

    // //     myfile[0].open("/tmp/perceptron0");
    // //     myfile[1].open("/tmp/perceptron1");
 
    // //     for(size_t i = 0; i < data.first.size(); i++){
    // //         VecNf X = data.first[i];
    // //         std::vector<VecNf> in = {X};
    // //         std::vector<VecNf> * out;
            

    // //         model.fast_forward(in, out);

    // //         bool result = (*out)[0][0] > 0.5f;

    // //         myfile[result] << X[0] << "\t" << X[1] << "\n";

            
    // //     }
    // //     myfile[0].close();
    // //     myfile[1].close();

    // //     GnuplotPipe gp;

    // //     std::stringstream ss;

    // //     ss << "set xrange [" << -5 << ": " << 5  << "];";
    // //     ss << "set yrange [" << -5 << ": " << 5  << "];";
    // //     std::string title = "IRIS2Class";
    // //     if (title.size() > 0)
    // //         ss << "set title \"" << title << "\"; show title;";
    // //     ss << "plot";
    // //     for (size_t i = 0; i < 2; i++)
    // //     {
    // //         std::stringstream fname;
    // //         fname << "/tmp/perceptron" << i ;
    // //         ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
    // //         ss << " title \"ID: " << i << "\"";
            
    // //         // if (i < (2 - 1))
    // //             ss << ", ";
    // //     }
    // //     auto W = d.getWeights(0);
    // //     ss << " " << -W[2]/W[1] << " " << std::showpos << (-(W[2]/W[1]) / (W[2]/W[0]) )  << "*x title \"IRIS2Class Decision Boundary\"; ";
    // //     std::cout << ss.str() << "\n";
    // //     gp.sendLine(ss.str());        


    // // }
    // std::cout << "Learning IRIS" << std::endl;
    // {
    //     auto X = Datasets<float>::IRIS::X();
    //     auto Y = Datasets<size_t>::IRIS::Y();

    //     auto lda_data = Matrix2D<float>(X);
    //     auto lda_y = Matrix2D<size_t>(Y);

    //     auto lda = LDA<float>(lda_data, lda_y, Datasets<float>::IRIS::nb_class(), 2);

    //     auto t_data = lda.transform(lda_data);

    //     std::pair< std::vector<VecNf>, std::vector<VecNf> > data;

    //     auto y_it = Y.begin();
    //     for(size_t i =0 ; i < t_data.shape[0]; i++){
    //         VecNf y_rat = VecNf::zeros(Datasets<float>::IRIS::nb_class());
    //         y_rat[(*y_it)[0]] = 1.f;
    //         data.first.push_back(VecNf::from_vector({t_data[i][0], t_data[i][1] }));
    //         data.second.push_back(y_rat);
    //         y_it++;
    //     }
        
    //     PlaceHolder a(data.first[0].size());
        
    //     Dense d(Datasets<float>::IRIS::nb_class(), sigmoid, a);
        
    //     Sequential model(d);
        
    //     SGD(&model, data.first, data.second, 2000, 10,0.01f);


    //     // plot IRIS results
    //     model.setBatchSize(1);

    //     std::ofstream myfile[3];

    //     std::remove("/tmp/perceptron0");
    //     std::remove("/tmp/perceptron1");
    //     std::remove("/tmp/perceptron2");

    //     myfile[0].open("/tmp/perceptron0");
    //     myfile[1].open("/tmp/perceptron1");
    //     myfile[2].open("/tmp/perceptron2");
    //     float tot = 0;
    //     for(size_t i = 0; i < data.first.size(); i++){
    //         VecNf X = data.first[i];
    //         std::vector<VecNf> in = {X};
    //         std::vector<VecNf> * out;
            

    //         model.fast_forward(in, out);

    //         auto o = {(*out)[0][0], (*out)[0][1], (*out)[0][2]};
    //         int argMax = std::distance(o.begin(), std::max_element(o.begin(), o.end()));

    //         myfile[argMax] << X[0] << "\t" << X[1] << "\n";

    //         tot += (Y[i][0] == argMax);
            
    //     }

    //     myfile[0].close();
    //     myfile[1].close();
    //     myfile[2].close();

    //     GnuplotPipe gp;

    //     std::stringstream ss;

    //     ss << "set xrange [" << -5 << ": " << 5  << "];";
    //     ss << "set yrange [" << -5 << ": " << 5  << "];";
    //     std::string title = "IRIS";
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";
    //     ss << "plot";
    //     for (size_t i = 0; i < Datasets<float>::IRIS::nb_class(); i++)
    //     {
    //         std::stringstream fname;
    //         fname << "/tmp/perceptron" << i ;
    //         ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
    //         ss << " title \"ID: " << i << "\"";
            
    //         // if (i < (2 - 1))
    //             ss << ", ";
    //     }
    //     // for (size_t i = 0; i < Datasets<float>::IRIS::nb_class(); i++)
    //     // {
    //     //     auto W = d.getWeights(i);

    //     // ss << " " << -W[2]/W[1] << " " << std::showpos << (-(W[2]/W[1]) / (W[2]/W[0]) )  << "*x title \"IRIS Decision Boundary\", ";
    //     // }
    //     std::cout << ss.str() << "\n";
    //     gp.sendLine(ss.str());        
    //     std::cout << "Accuracy: " << tot/data.first.size()  << "\n";

    // }

//  std::cout << "Learning IRIS" << std::endl;
//     {
//         auto X = Datasets<float>::IRIS::X();
//         auto Y = Datasets<size_t>::IRIS::Y();

        
//         std::pair< std::vector<VecNf>, std::vector<VecNf> > data;

//         auto y_it = Y.begin();
//         for(size_t i =0 ; i < X.size(); i++){
//             VecNf y_rat = VecNf::zeros(Datasets<float>::IRIS::nb_class());
//             y_rat[(*y_it)[0]] = 1.f;
//             data.first.push_back(VecNf::from_vector(X[i]));
//             data.second.push_back(y_rat);
//             y_it++;
//         }
        
//         PlaceHolder a(data.first[0].size());
        
//         Dense d(Datasets<float>::IRIS::nb_class(), sigmoid, a);
        
//         Sequential model(d);
        
//         SGD(&model, data.first, data.second, 2000, 10, 0.01f);


//         // EVALUATE IRIS results
//         model.setBatchSize(1);

//         float tot = 0;
//         for(size_t i = 0; i < data.first.size(); i++){
//             VecNf X = data.first[i];
//             std::vector<VecNf> in = {X};
//             std::vector<VecNf> * out;    

//             model.fast_forward(in, out);

//             auto o = {(*out)[0][0], (*out)[0][1], (*out)[0][2]};
//             int argMax = std::distance(o.begin(), std::max_element(o.begin(), o.end()));

//             tot += (Y[i][0] == argMax);
            
//         }

//         std::cout << "Accuracy: " << tot/data.first.size()  << "\n";

//     }   
    return 0;
}
