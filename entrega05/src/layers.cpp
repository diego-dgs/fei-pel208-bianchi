#include "layers.h"
#include <assert.h>


// Layer Dense
Dense::Dense(size_t nun_neurons, void(*pactivation)( VecNf &, bool), Layer &lin)
{
    this->num_inputs = lin.getOutputSize();
    this->num_outputs = nun_neurons + 1;
    this->num_neurons = nun_neurons;
    this->weights = std::vector<VecNf>(this->num_neurons);
    for (auto& it : weights){
        it = VecNf::rnd(this->num_inputs);
    }
    this->ew = std::vector<VecNf>(this->weights.size());
    for(auto& it : ew){
        it = VecNf::zeros(this->num_inputs);
    }

    this->activation = pactivation;
    this->add_input(lin);
}

void Dense::forward( std::vector<VecNf> &in, std::vector<VecNf> * &out)
{

#ifdef USE_OPENMP
omp_set_num_threads(4);
#pragma omp parallel
{
#endif

    auto t_data = ThreadDef(in.size());



    VecNf * ret_it = &v_output[0];
    VecNf * it = &in[0];


    for(size_t k = t_data.thread_data_ini ; k < t_data.thread_data_end ; ++k){

        for(size_t i = 0; i < this->num_neurons; ++i ){
            ret_it[k][i] = it[k].dot(this->weights[i]);
        }
        ret_it[k][this->num_outputs -1] = 0.f;

        this->activation(ret_it[k], false);
        ret_it[k][this->num_outputs -1] = 1.f;

    }
#ifdef USE_OPENMP
     }
#endif

    out =  &this->v_output;
}

void Dense::backward( std::vector<VecNf> &err, std::vector<VecNf> * &out)
{
#ifdef USE_OPENMP
omp_set_num_threads(4);
#pragma omp parallel
{
#endif

    auto t_data = ThreadDef(err.size());

    auto err_it = err.begin();
    auto output_it = v_output.begin();
    auto dt_it = v_deltas.begin();
    auto ret_it= v_error.begin();


    for(size_t k = t_data.thread_data_ini ; k < t_data.thread_data_end ; ++k){
        // (*(err_it+k)).print_vector();
        auto aux = *(output_it+k);
        this->activation(aux, true);
        *(dt_it+k)  = aux  * *(err_it+k);
        // (*(dt_it+k)).print_vector("Delta");

        (*(ret_it+k)) = 0;
        for(size_t i = 0 ; i < this->num_neurons; ++i){
            (*(ret_it+k)) += (this->weights[i] * (*(dt_it+k))[i] )  ;
        }
        // (*(ret_it+k)).print_vector("Erro propagado");

    }
#ifdef USE_OPENMP
     }
#endif
    out =  &v_error;
}

void Dense::fast_forward( std::vector<VecNf> &in, std::vector<VecNf> * &out)
{
    auto ret_it = v_output.begin();
    for( auto& it : in ){
        for(size_t i = 0; i < this->num_neurons; ++i ){
            (*ret_it)[i] = it.dot(this->weights[i]);
        }
        this->activation(*ret_it, false);
        ret_it++;
    }
    out = &v_output;

}

void Dense::update_weights( std::vector<VecNf> &in, float l_rate, std::vector<VecNf> * &out)
{
#ifdef USE_OPENMP
#pragma omp parallel for schedule(static) num_threads(4)
#endif
    for(size_t i = 0 ; i < ew.size(); i ++){
        ew[i] = 0;
    }
    auto dt_it = v_deltas.begin();
    for( auto& it : in ){
#ifdef USE_OPENMP
#pragma omp parallel for schedule(static) num_threads(4)
#endif
        for(size_t i = 0 ; i < this->num_neurons; ++i){
            //weight = weight + learning_rate * error * input
            // ew[i].print_vector("Update");
            ew[i] += (it * ((*dt_it)[i] * l_rate)) ;
            // it.print_vector("Inputs");
        }
        dt_it++;
    }
#ifdef USE_OPENMP
#pragma omp parallel for schedule(static) num_threads(4)
#endif
    for(size_t i = 0 ; i < this->num_neurons; ++i){
        //weight = weight + learning_rate * error * input
        // ew[i].print_vector("Update");
        // this->weights[i].print_vector("Wt");
        this->weights[i] += (ew[i]);
        // ew[i].print_vector("ew");
    }

    out =  &this->v_output;
}

void Dense::setBatchSize(size_t sz)
{
    if(this->v_error.size() != sz)
        this->v_error = std::vector<VecNf>(sz);
    for(size_t i = 0 ; i < v_error.size(); ++i){
        v_error[i] = VecNf::zeros(num_inputs);
    }

    if(this->v_deltas.size() != sz)
        this->v_deltas = std::vector<VecNf>(sz);
    if (this->v_output.size() != sz)
        this->v_output = std::vector<VecNf>(sz);

    for(size_t i = 0 ; i < v_output.size(); ++i){
        v_output[i] = VecNf::zeros(num_outputs);
    }
    for(size_t i = 0 ; i < v_deltas.size(); ++i){
        v_deltas[i] = VecNf::zeros(num_neurons);
    }
}




PlaceHolder::PlaceHolder(size_t num)
{
    this->num_inputs = num;
    this->num_outputs = num+1;
    this->input_layer = nullptr;


}

void PlaceHolder::backward( std::vector<VecNf> &in, std::vector<VecNf> * &out)
{

    out =  reinterpret_cast<std::vector<VecNf> *>(reinterpret_cast<uintptr_t>(&in));
}

void PlaceHolder::fast_forward( std::vector<VecNf> &in, std::vector<VecNf> * &out)
{
#ifdef USE_OPENMP

#pragma omp parallel
{
#endif

    auto t_data = ThreadDef(in.size());


    VecNf * it = &appended[0];
    VecNf * i = &in[0];

    for(size_t k = t_data.thread_data_ini ; k < t_data.thread_data_end ; ++k){

        std::copy(&(i[k][0]), &(i[k][0]) + i[k].size(),  &(it[k])[0] );

        it[k][i[k].size()] = 1.f;
    }
#ifdef USE_OPENMP
     }
#endif
    out =  &appended;
}

void PlaceHolder::forward( std::vector<VecNf> &in, std::vector<VecNf> * &out)
{

#ifdef USE_OPENMP

#pragma omp parallel
{
#endif

    auto t_data = ThreadDef(in.size());


    VecNf * it = &appended[0];
    VecNf * i = &in[0];

    for(size_t k = t_data.thread_data_ini ; k < t_data.thread_data_end ; ++k){

        std::copy(&(i[k][0]), &(i[k][0]) + i[k].size(),  &(it[k])[0] );

        it[k][i[k].size()] = 1.f;
    }
#ifdef USE_OPENMP
     }
#endif
    out =  &appended;

}

void PlaceHolder::update_weights( std::vector<VecNf> &in, float l_rate, std::vector<VecNf> * &out)
{
#ifdef USE_OPENMP

#pragma omp parallel
{
#endif

    auto t_data = ThreadDef(in.size());


    VecNf * it = &appended[0];
    VecNf * i = &in[0];

    for(size_t k = t_data.thread_data_ini ; k < t_data.thread_data_end ; ++k){

        std::copy(&(i[k][0]), &(i[k][0]) + i[k].size(),  &it[k][0] );

        it[k][i[k].size()] = 1.f;
    }
#ifdef USE_OPENMP
     }
#endif
    out =  &appended;
}

void PlaceHolder::setBatchSize(size_t sz)
{
    this->appended = std::vector<VecNf>(sz);
    for(auto& it: appended){
        it = VecNf::zeros(num_inputs+1);
    }
}


void Sequential::forward( std::vector<VecNf>& in, std::vector<VecNf> * &out){
    auto p = this->first_layer;
    std::vector<VecNf> *d1;
    std::vector<VecNf> *d2;
    d1 = &in;
    while(p != nullptr){
        p->forward(*d1, d2);
        d1 = d2;
        p = p->getNextLayer();
    }
    out = d2;
}
void Sequential::backward( std::vector<VecNf>& delta, std::vector<VecNf> * &out){
    auto p = this->last_layer;
    std::vector<VecNf> *d1;
    std::vector<VecNf> *d2;
    d1 = &delta;
    while(p != nullptr){
        p->backward(*d1, d2);
        d1 = d2;
        p = p->getPreviousLayer();
    }
    out = d2;

}
void Sequential::fast_forward( std::vector<VecNf>& in, std::vector<VecNf> * &out){
    auto p = this->first_layer;
    std::vector<VecNf> *d1;
    std::vector<VecNf> *d2;
    d1 = &in;
    while(p != nullptr){
        p->fast_forward(*d1, d2);
        d1 = d2;
        p = p->getNextLayer();
    }
    out = d2;
}

void Sequential::update_weights( std::vector<VecNf>& in, float l_rate, std::vector<VecNf> * &out ){
    auto p = this->first_layer;
    std::vector<VecNf> *d1;
    std::vector<VecNf> *d2;
    d1 = &in;
    while(p != nullptr){
        p->update_weights(*d1, l_rate, d2);
        d1 = d2;
        p = p->getNextLayer();
    }
    out = d2;

}

void Sequential::setBatchSize(size_t sz)
{
    Layer* p = this->first_layer;
    while(p != nullptr){
        p->setBatchSize(sz);
        p = p->getNextLayer();
    }

}
