#include "losses.h"



void loss_derivate(const std::vector<VecNf> &net_out, const std::vector<VecNf> &expected, std::vector<VecNf> &ret)
{
    auto X = net_out.begin();
    auto Y = expected.begin();
    auto ER = ret.begin();
#ifdef USE_OPENMP
#pragma omp parallel for schedule(static)
#endif
    for(size_t ii = 0 ; ii < net_out.size(); ii++){
        *(ER+ii) =  *(Y+ii) - *(X+ii);
        // *(ER+ii) *= -1;
        // (*(ER+ii)).print_vector("Error: ");
    }
}
