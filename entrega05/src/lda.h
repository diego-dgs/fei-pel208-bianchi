#pragma once

#include "matrix2d.h"
#include "least_squares.h"
#include "gnuplot.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>

template <class T>
class LDA
{

private:
    LDA() {}

public:
    Matrix2D<T> mean;
    Matrix2D<size_t> _Y;
    Matrix2D<T> featurek, centreded_data;
    size_t m_nb_class;
    std::vector<Matrix2D<T>> coefs;
    std::vector<Matrix2D<T>> intercept;

    LDA(const Matrix2D<T> &data, const Matrix2D<size_t> &Y, const size_t &nb_class, const size_t &N) : mean({1, 1}), featurek({1, 1}), centreded_data({1, 1}), _Y(Matrix2D<size_t>::zeros({1, 1}))
    {
        auto D = data;
        auto grand_mean = D.mean(0);
        this->mean = grand_mean;
        this->_Y = Y;
        this->centreded_data = D - this->mean;
        this->m_nb_class = nb_class;

        std::vector<Matrix2D<T>> per_class_data = split_by_class(data, Y, nb_class);

        std::vector<Matrix2D<T>> per_class_mean;
        for (size_t k = 0; k < nb_class; k++)
        {
            per_class_mean.push_back(per_class_data[k].mean(0));
        }
        for (size_t k = 0; k < nb_class; k++)
        {
            std::cout << "Class mean "<< k << ":\n";
            per_class_mean[k].print_matrix();
        }


        std::vector<Matrix2D<T>> per_class_cov;
        for (size_t k = 0; k < nb_class; k++)
        {
            auto centreded_data = per_class_data[k] - per_class_mean[k];
            auto cT = centreded_data.transpose();
            auto COVm = cT.dot(centreded_data);
            per_class_cov.push_back(COVm);
        }

        Matrix2D<T> Sw = Matrix2D<T>::zeros(per_class_cov[0].shape);
        for (size_t k = 0; k < nb_class; k++)
        {
            Sw = Sw + (per_class_cov[k]);
        }

        std::cout << "within-class Scatter Matrix :\n";
        Sw.print_matrix();
        Matrix2D<T> Sb = Matrix2D<T>::zeros(per_class_cov[0].shape);
        for (size_t k = 0; k < nb_class; k++)
        {
            auto kmeanT = (per_class_mean[k] - grand_mean).transpose();
            auto dotM = kmeanT.dot(per_class_mean[k]);
            auto scaledM = dotM * per_class_data[k].shape[0];
            Sb = Sb + (scaledM);
        }

        std::cout << "between-class Scatter Matrix:\n";
        Sb.print_matrix();

        auto SwInv = Sw.inv();
        auto fM = SwInv.dot(Sb);

        Matrix2D<T> Evectors({1, 1});
        Matrix2D<T> Evalues({1, 1});

        fM.getEigenvectorEigenvaluesFromLibEigen(Evectors, Evalues);
        Evectors.print_matrix();
        Evalues.print_matrix();
        featurek = topKFeatureVector(Evalues, Evectors, N);

        for (size_t i = 0; i < per_class_mean.size(); i++)
        {
            // self.coef_ = np.dot(self.means_, evecs).dot(evecs.T)
            auto fT = featurek.transpose();
            auto c = per_class_mean[i].dot(featurek);
            c = c.dot(fT);
            coefs.push_back(c);
        }
        for (size_t i = 0; i < per_class_mean.size(); i++)
        {

            // self.intercept_ = (-0.5 * np.diag(np.dot(self.means_, self.coef_.T)) +
            //                np.log(self.priors_))
            T aux = -0.5;
            auto ct = coefs[i].transpose();
            auto it = per_class_mean[i].dot(ct);
            it = it * aux;
            intercept.push_back(it);
        }
        for (size_t i = 0; i < per_class_mean.size(); i++)
        {
            coefs[i].print_matrix();
            intercept[i].print_matrix();
        }
        // centreded_data = data - mean;
        // auto cT = centreded_data.transpose();
        // auto COVm = cT.dot(centreded_data) * (1.0 / static_cast<T>(D.shape[0] - 1));

        // Matrix2D<T> Evectors({1, 1});
        // Matrix2D<T> Evalues({1, 1});

        // COVm.symmetricMatrixEigenVectorsAndValues(Evectors, Evalues);

        // featurek = topKFeatureVector(Evalues, Evectors, nb_class - 1);

        // auto TT = transform(D);
        // //        D.print_matrix();
        // //        TT.print_matrix();
        // auto ID = inverse_transform(TT);
        // //        ID.print_matrix();
        // std::cout << D.almostClose(ID, 1e-7) << "\n";
    }

    std::vector<Matrix2D<T>> split_by_class(const Matrix2D<T> &data, const Matrix2D<size_t> &Y, const size_t &nb_class)
    {
        auto X = data;
        auto y = Y;
        std::vector<Matrix2D<T>> per_class_data;
        for (size_t k = 0; k < nb_class; k++)
        {
            std::vector<std::vector<T>> class_data;
            for (size_t i = 0; i < y.shape[0]; i++)
            {
                if (k == y[i][0])
                {
                    std::vector<T> line;
                    for (size_t j = 0; j < X.shape[1]; j++)
                        line.push_back(X[i][j]);

                    class_data.push_back(line);
                }
            }
            per_class_data.push_back(Matrix2D<T>(class_data));
        }
        return per_class_data;
    }

    void plotPoints2d(Matrix2D<T> &data1, Matrix2D<size_t> &Y, const std::vector<std::string> &class_names = {}, const std::string &title = "")
    {
        for (size_t k = 0; k < m_nb_class; k++)
        {
            std::ofstream myfile;
            std::stringstream fname;
            fname << "/tmp/lda" << k << ".dat";
            std::remove(fname.str().c_str());
            myfile.open(fname.str().c_str());

            for (size_t i = 0; i < data1.shape[0]; i++)
            {
                if (Y[i][0] == k)
                {
                    for (size_t j = 0; j < data1.shape[1]; j++)
                        myfile << data1[i][j] << "\t";
                    myfile << Y[i][0] << "\n";
                }
            }
            myfile.close();
        }
        GnuplotPipe gp;

        std::stringstream ss;

        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";
        ss << "plot";
        for (size_t i = 0; i < m_nb_class; i++)
        {
            std::stringstream fname;
            fname << "/tmp/lda" << i << ".dat";
            ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            if (class_names.size() != 0)
            {
                ss << " title \"" << class_names[i] << "\"";
            }
            if (i < (m_nb_class - 1))
                ss << ", ";
        }
        gp.sendLine(ss.str());
    }

    void plotBoundw2d(Matrix2D<T> &data1, Matrix2D<size_t> &Y, const std::vector<std::string> &class_names = {}, const std::string &title = "")
    {
        for (size_t k = 0; k < m_nb_class; k++)
        {
            std::ofstream myfile;
            std::stringstream fname;
            fname << "/tmp/lda" << k << ".dat";
            std::remove(fname.str().c_str());
            myfile.open(fname.str().c_str());

            for (size_t i = 0; i < data1.shape[0]; i++)
            {
                if (Y[i][0] == k)
                {
                    for (size_t j = 0; j < data1.shape[1]; j++)
                        myfile << data1[i][j] << "\t";
                    myfile << Y[i][0] << "\n";
                }
            }
            myfile.close();
        }
        GnuplotPipe gp;
        
        T x_min = 1e19;
        for(size_t k = 0; k < data1.shape[0]; k ++){
            if(data1[k][0] < x_min)
                x_min = data1[k][0];
        }
        T x_max = -1e19;
        for(size_t k = 0; k < data1.shape[0]; k ++){
            if(data1[k][0] > x_max)
                x_max = data1[k][0];
        }

        T y_min = 1e19;
        for(size_t k = 0; k < data1.shape[0]; k ++){
            if(data1[k][1] < y_min)
                y_min = data1[k][1];
        }
        T y_max = -1e19;
        for(size_t k = 0; k < data1.shape[0]; k ++){
            if(data1[k][1] > y_max)
                y_max = data1[k][1];
        }

        std::stringstream ss;
        ss << "set xrange [" << x_min - std::abs(x_min) << ": " << x_max + std::abs(x_max)  << "];";
        ss << "set yrange [" << y_min - std::abs(y_min)   << ": " << y_max + std::abs(y_max)  << "];";

        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";
        ss << "plot";
        for (size_t i = 0; i < m_nb_class; i++)
        {
            std::stringstream fname;
            fname << "/tmp/lda" << i << ".dat";
            ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            if (class_names.size() != 0)
            {
                ss << " title \"" << class_names[i] << "\"";
            }

            ss << ", ";
        }



        T one = 1.0;
        auto m = one/(featurek[0][1] / featurek[0][0]);
        
        T intercetp = -m * mean[0][0] + mean[0][1];
        y_min = std::max(x_min *m + intercetp, y_min );
        y_max = std::min(x_max *m + intercetp, y_max );

        x_max = (-intercetp + y_max)/m;
        x_min = (-intercetp + y_min)/m;

        std::ofstream myfile;
        std::stringstream fname;
        fname << "/tmp/lda_vector.dat";
        std::remove(fname.str().c_str());
        myfile.open(fname.str().c_str());
        myfile << x_min << " " << y_min << " ";
        myfile << x_max << " " << y_max << "\n";

        myfile.close();
        ss << "\"" << fname.str() << "\" using 1:2:($3-$1):($4-$2) with vectors nohead title \"boundary\"";
        
        // ss << " " << std::showpos << intercetp << " " << m << "*x title \"LDA Principal Component\"; ";
        gp.sendLine(ss.str());
    }

    
    void plotPrincipalComponentMeanData(const std::string &title = "")
    {
        std::ofstream myfile;
        std::remove("/tmp/lda.dat");
        myfile.open("/tmp/lda.dat");
        for (size_t i = 0; i < centreded_data.shape[0]; i++)
        {
            for (size_t j = 0; j < centreded_data.shape[1]; j++)
                myfile << centreded_data[i][j] << "\t";
            myfile << _Y[i][0] << "\n";
        }
        myfile.close();
        GnuplotPipe gp;

        std::stringstream ss;
        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";

        ss << "plot \"/tmp/lda.dat\" using 1:2:3 with points palette title \""
           << "Mean Data"
           << "\", ";

        ss << " " << std::showpos << featurek[0][1] / featurek[0][0] << "*x title \"LDA Principal Component\"; ";

        gp.sendLine(ss.str());
    }

    void plotPrincipalComponentOriginalData(const std::string &title = "")
    {
        std::ofstream myfile;
        std::remove("/tmp/lda.dat");
        myfile.open("/tmp/lda.dat");

        auto ori_data = centreded_data + mean;

        for (size_t i = 0; i < centreded_data.shape[0]; i++)
        {
            for (size_t j = 0; j < centreded_data.shape[1]; j++)
                myfile << centreded_data[i][j] << "\t";
            myfile << _Y[i][0] << "\n";
        }
        myfile.close();
        GnuplotPipe gp;

        std::stringstream ss;

        T intercetp = -featurek[0][1] / featurek[0][0] * mean[0][0] + mean[0][1];
        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";
        ss << "plot \"/tmp/lda.dat\" using 1:2:3 with points palette title \""
           << "Original Data"
           << "\", ";

        ss << " " << std::showpos << intercetp << " " << featurek[0][1] / featurek[0][0] << "*x title \"LDA Principal Component\"; ";

        gp.sendLine(ss.str());
    }

    Matrix2D<T> transform(Matrix2D<T> &data)
    {
        auto ajust_dataT = (data - mean).transpose();
        auto featurekT = featurek.transpose();
        return featurekT.dot(ajust_dataT).transpose();
    }

    Matrix2D<T> inverse_transform(Matrix2D<T> &data)
    {
        auto dataT = data.transpose();
        auto ajust_data = featurek.transpose().pseudo_inv().dot(dataT).transpose();
        return ajust_data + mean;
    }

    Matrix2D<T> topKFeatureVector(Matrix2D<T> &Evalues, Matrix2D<T> &Evectors, const size_t &N)
    {

        std::vector<std::pair<T, size_t>> to_order;

        std::vector<Matrix2D<T>> vecs;
        for (size_t i = 0; i < Evalues.shape[0]; i++)
        {

            Matrix2D<T> vec({Evectors.shape[0], 1});
            for (size_t j = 0; j < Evectors.shape[0]; j++)
            {
                vec[j][0] = Evectors[j][i];
            }

            to_order.push_back(std::make_pair(Evalues[i][0], i));
            vecs.push_back(vec);
        }
        // lamba function to order feature vector
        std::sort(to_order.begin(), to_order.end(), [](const std::pair<T, size_t> &a, const std::pair<T, size_t> &b) {
            return (a.first > b.first);
        });

        Matrix2D<T> featurek({Evectors.shape[0], N});

        for (size_t i = 0; i < N; i++)
        {
            for (size_t j = 0; j < Evectors.shape[0]; j++)
            {

                featurek[j][i] = vecs[to_order[i].second][j][0];
            }
        }

        return featurek;
    }

    virtual ~LDA()
    {
    }
};

//GnuplotPipe gp;
//gp.sendLine("plot  (1 2 3 4) (1 4 9 16)");
