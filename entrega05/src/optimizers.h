#ifndef OPTIMIZERS_H
#define OPTIMIZERS_H
#include <vector>
#include <vecnf.h>
#include "layers.h"

void SGD(Layer* net,
        const std::vector<VecNf> &X_train,
         const std::vector<VecNf> &Y_train,
         size_t epoch,
         size_t batch_size,
         float l_rate,
         const std::vector<VecNf> &X_val = {},
         const std::vector<VecNf> &Y_val ={});

#endif // OPTIMIZERS_H
