#include "optimizers.h"
#include <vector>
#include <map>
#include <set>
#include <vecnf.h>
#include <random>
#include <iostream>
#include <losses.h>
#include <chrono>



inline void cvtStdVector2Vecnf(const std::vector<std::vector<float> > & in ,  std::vector < VecNf >& out){
    auto xl = out.begin();
    for(auto xp = in.begin(); xp != in.end(); xp++, xl++){
        (*xl) = VecNf(xp->size(), xp->data());
    }
}



std::vector<std::pair<std::vector<VecNf >, std::vector<VecNf >> > splitInBatchs(size_t batch_size, const std::vector<VecNf > &X_train, const std::vector<VecNf > &Y_train){
    size_t num_batchs = X_train.size()/batch_size;
    std::vector<std::pair<std::vector<VecNf >, std::vector<VecNf >> > batchs(num_batchs);

    std::set <size_t> full;

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_int_distribution<> dis(0, num_batchs-1);

    for(size_t i = 0 ; i < X_train.size(); i++){
        size_t irnd = dis(gen);

        while(full.find(irnd) != full.end()){
            irnd++;
            if(irnd>=num_batchs)irnd=0;
            if(full.size() >= num_batchs )break;

        }
        if(full.size() >= num_batchs )break;
        batchs[irnd].first.push_back(X_train[i]);
        batchs[irnd].second.push_back(Y_train[i]);
        if(batchs[irnd].first.size() == batch_size){
            full.insert(irnd);
        }
    }
    return batchs;

}

inline size_t argmax( VecNf &v, size_t sz){
    size_t arg = 0;
    for(size_t i = 1 ; i < sz; i++ ){
        if(v[i] > v[arg]){
            arg = i;
        }
    }
    return arg;
}

float calc_accuracity_logits( std::vector<VecNf > &X, std::vector<VecNf > &Y){
    float tot = X.size();
    float eq = 0;

    for(int i = 0 ; i < tot; i++){
        if(argmax(X[i],X[i].size()) == argmax(Y[i],X[i].size())){
            eq+=1.0;
        }
    }
    return eq/tot;
}

float calc_accuracity( std::vector<VecNf > &X, std::vector<VecNf > &Y){
    float tot = X.size();
    float eq = 0;

    for(int i = 0 ; i < tot; i++){
        if(X[i][0] > 0.5f && Y[i][0] > 0.5f){
            eq+=1.0;
        }
        if(X[i][0] < 0.5f && Y[i][0] < 0.5f){
            eq+=1.0;
        }
    }
    return eq/tot;
}

void show_progress(const std::vector<VecNf > &err,size_t epoch, size_t batch, float rate, float accuracity = -1){
    static std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
    
    std::chrono::steady_clock::time_point stop = std::chrono::steady_clock::now();
    float error = 0;
    for(auto er : err){
        error+= std::sqrt(er.dot(er));
    }
    error = error/float(err.size());

    if(epoch % 5 == 0){
        std::cout << "Epoch: "<< epoch << " -- [";
        for(size_t i =0 ;i < batch; i ++)
            std::cout << "=";
        std::cout << ">";
        // for(size_t i =batch ;i < 60000/err.size(); i ++)
        //     std::cout << " ";
    
        std::cout << "];\nError: " << error << "; Acc: " << accuracity << "; Rate: " << rate << "; Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(stop - start).count() << "[ms]" << std::endl;
    }
    start = std::chrono::steady_clock::now();
}

void SGD(Layer* net, const std::vector<VecNf > &X_train, const std::vector<VecNf > &Y_train,size_t epochs, size_t batch_size, float l_rate, const std::vector<VecNf > &X_val, const std::vector<VecNf > &Y_val)
{
    net->setBatchSize(batch_size);
    std::vector<VecNf > losses(batch_size);
    for(auto& it : losses){
        it = VecNf::zeros(Y_train[0].size()+1);
    }


    PlaceHolder expected(Y_train[0].size());
    expected.setBatchSize(batch_size);
    for(size_t epoch = 1; epoch < epochs+1; epoch++ ){
        auto batchs_pair = splitInBatchs(batch_size, X_train,Y_train);
        int i = 0;
        for(auto &batch : batchs_pair){
            std::vector<VecNf >* data,*dataY,*errp;
            net->forward(batch.first, data);
            

            expected.forward(batch.second, dataY);
            loss_derivate(*data, *dataY, losses);

            
            show_progress(losses, epoch, i, l_rate, calc_accuracity(batch.second, *data) );


            net->backward(losses, errp);


            net->update_weights(batch.first, l_rate, data);
            i++;
        }
        // std::cout << "Finish epoch " << epoch << std::endl;
        if(epoch %1000 == 0)
            l_rate *=0.9;

        }
}
