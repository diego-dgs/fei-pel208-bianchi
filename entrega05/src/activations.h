#ifndef ACTIVATIONS_H
#define ACTIVATIONS_H

#include "vecnf.h"
#include "float.h"


inline void sigmoid ( VecNf& v, bool derivate){
    if(derivate){
        VecNf ret = VecNf::zeros(v.size());
        for(size_t i = 0 ; i < ret.size(); i++){
            ret[i] = v[i]*((v[i] -1)*-1);
        }
        v = ret;
    }else{

        VecNf ret = VecNf::zeros(v.size());
        for(size_t i = 0 ; i < ret.size(); i++){
            ret[i] = 1.f/(1.f+ std::exp(-v[i]));
        }
        v = ret;
    }
}

inline void linear (VecNf& v, bool derivate){
    if(derivate){
        VecNf ret = VecNf::ones(v.size());

        v = ret;
    }
}


inline void relu (VecNf& v, bool derivate){
    if(derivate){
//        for(int i = 0 ; i < v.size(); i++){

//            if(v[i] > 0)
//                v[i] = 1;
//            else
//                v[i] = 0;
//        }
        v *= (1.f/FLT_EPSILON);
        v = VecNf::min(v, 1.f);
        v = VecNf::max(v, 0.0f);

    }else{
         v = VecNf::max(v,0);
    }
}
//inline VecNf relu (const VecNf&v, bool derivate){
//    if(derivate){
//        VecNf ret = VecNf::zeros(v.size());
//        for(int i = 0 ; i < ret.size(); i++){

//            if(v[i] > 0)
//                ret[i] = 1;
//            else
//                ret[i] = 0;
//        }
//        return ret;

//    }else{

//        VecNf ret = VecNf::zeros(v.size());
//        for(int i = 0 ; i < ret.size(); i++){
//            if(v[i] > 0)
//                ret[i] = v[i];
//            else
//                ret[i] = 0;
//        }
//         return ret;
//    }
//}

inline void softmax ( VecNf& v, bool derivate){
    if(derivate){
        v *= (1.f/0.0001);
        v = VecNf::min(v, 1.f);
        v = VecNf::max(v, 0.0f);
    }else{
        float max_v = VecNf::maximun(v);
        v -=  max_v;
        v = VecNf::exp(v);
        float sum = VecNf::sum(v);
        v *= (1.f/(sum));
    }
}



#endif // ACTIVATIONS_H
