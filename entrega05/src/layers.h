#ifndef LAYERS_H
#define LAYERS_H

#include "vecnf.h"
#include <vector>
#include <list>
#include <functional>
#include <omp.h>

class ThreadDef{
public:
    size_t thread_id ;
    size_t num_threads ;
    size_t thread_data_sz ;
    size_t thread_data_ini ;
    size_t thread_data_end ;

    ThreadDef(size_t sz){
        thread_id = omp_get_thread_num();
        num_threads = omp_get_num_threads();
        thread_data_sz = sz/num_threads;
        thread_data_ini = thread_id * thread_data_sz;
        if(thread_id != (num_threads-1)){
            thread_data_end = std::min((thread_id+1) * thread_data_sz, sz);
        }else{
            thread_data_end =sz;
        }
    }
};


class Layer{
public:
    Layer(){
        this->input_layer = nullptr;
        this->output_layer = nullptr;
    }

    virtual void fast_forward( std::vector<VecNf>& in, std::vector<VecNf> * &out)=0;
    virtual void forward( std::vector<VecNf>& in, std::vector<VecNf> * &out)=0;
    virtual void backward( std::vector<VecNf>& in, std::vector<VecNf> * &out)=0;
    virtual void update_weights( std::vector<VecNf>& in, float l_rate , std::vector<VecNf> * &out)=0;
    virtual void setBatchSize(size_t sz)=0;
    void add_input(Layer& lin){
        this->input_layer = &lin;
        lin.set_output(*this);
    }
    void set_output(Layer& out){
        this->output_layer = &out;
    }
    size_t getOutputSize() const{
        return this->num_outputs;
    }
    Layer * getPreviousLayer()const{
        return this->input_layer;
    }
    Layer * getNextLayer()const{
        return this->output_layer;
    }
protected:
    size_t num_inputs;
    size_t num_outputs;
    Layer*  input_layer;
    Layer* output_layer;

};

class Sequential: public Layer{
public:
    Sequential( Layer& l){
        //find first layer;
        Layer * p0 =  &l;
        Layer * p1 =  p0->getPreviousLayer();
        while(p1 != nullptr){
            p0 = p1;
            p1 =  p0->getPreviousLayer();
        }
        this->first_layer = p0;
        p1 = p0->getNextLayer();
        while(p1 != nullptr){
            p0 = p1;
            p1 =  p0->getNextLayer();
        }
        this->last_layer = p0;
    }

    void forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void backward( std::vector<VecNf>& delta, std::vector<VecNf> * &out);
    void fast_forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void update_weights( std::vector<VecNf>& in, float l_rate , std::vector<VecNf> * &out);
    void setBatchSize(size_t sz);
private:
    Layer*  first_layer;
    Layer* last_layer;
};


class Dense :public Layer{
private:
    std::vector<VecNf> weights;
    std::vector<VecNf> v_output;
    std::vector<VecNf> v_deltas, v_error,ew;
    size_t num_neurons;
    void(*activation)( VecNf&, bool);

public:
    Dense(size_t nun_neurons,void(*pactivation)( VecNf&, bool), Layer &lin);
    void forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void backward( std::vector<VecNf>& delta, std::vector<VecNf> * &out);
    void fast_forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void update_weights( std::vector<VecNf>& in, float l_rate , std::vector<VecNf> * &out);
    void setBatchSize(size_t sz);
    VecNf getWeights(size_t neuron){
        return weights[neuron];
    }
};


class PlaceHolder :public Layer{
    std::vector<VecNf> appended;
public:
    PlaceHolder(size_t num);
    void forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void backward( std::vector<VecNf>& delta, std::vector<VecNf> * &out);
    void fast_forward( std::vector<VecNf>& in, std::vector<VecNf> * &out);
    void update_weights( std::vector<VecNf>& in, float l_rate , std::vector<VecNf> * &out);
    void setBatchSize(size_t sz);


};

#endif // LAYERS_H
