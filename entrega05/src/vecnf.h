#ifndef VECNF_H
#define VECNF_H
#pragma once

#include <malloc.h>
#include <memory>
#include <math.h>
#include <memory>
#include <random>
#include <omp.h>
#include <immintrin.h>
#include <avx_mathfun.h>
#include <x86intrin.h>
#include <float.h>
#include <iostream>

class VecNf;
typedef enum{
    ALLOC,
    DEALLOC
}MEMORY_OP;

void memory_handle(std::shared_ptr <float>& ptr , size_t len, MEMORY_OP op);



class ExpV{
public:
    const VecNf &V1;
    const VecNf &V2;
    inline ExpV(const VecNf& Va,const VecNf& Vb )
        : V1( Va ), V2( Vb ) {}
};

class sumV:public ExpV{
public:
    sumV(const  VecNf& Va,const  VecNf& Vb )
        : ExpV(Va,Vb) {}
};

sumV inline operator + (const  VecNf& Va,const  VecNf& Vb )
{
    return sumV( Va, Vb );
}


class minusV:public ExpV{
public:
    minusV(const  VecNf& Va,const  VecNf& Vb )
        : ExpV(Va,Vb) {}
};

minusV inline operator - (const  VecNf& Va,const  VecNf& Vb )
{
    return minusV( Va, Vb );
}

class mulV:public ExpV{
public:
    mulV(const  VecNf& Va,const  VecNf& Vb )
        : ExpV(Va,Vb) {}
};

mulV inline operator * (const  VecNf& Va,const  VecNf& Vb )
{
    return mulV( Va, Vb );
}


class divV:public ExpV{
public:
    divV(const  VecNf& Va,const  VecNf& Vb )
        : ExpV(Va,Vb) {}
};

divV inline operator / (const  VecNf& Va,const  VecNf& Vb )
{
    return divV( Va, Vb );
}


class ExpVf{
public:
    const VecNf &V1;
    float V2;
    inline ExpVf(const VecNf& Va,float Vb )
        : V1( Va ), V2( Vb ) {}
};

class sumVf:public ExpVf{
public:
    sumVf(const  VecNf& Va,  float Vb )
        : ExpVf(Va,Vb) {}
};

sumVf inline operator + (const  VecNf& Va,float Vb )
{
    return sumVf( Va, Vb );
}


class minusVf:public ExpVf{
public:
    minusVf(const  VecNf& Va,  float Vb )
        : ExpVf(Va,Vb) {}
};

minusVf inline operator - (const  VecNf& Va,float Vb )
{
    return minusVf( Va, Vb );
}



class mulVf:public ExpVf{
public:
    mulVf(const  VecNf& Va,float Vb )
        : ExpVf(Va,Vb) {}
};

mulVf inline operator * (const  VecNf& Va,float Vb )
{
    return mulVf( Va, Vb );
}


class divVf:public ExpVf{
public:
    divVf(const  VecNf& Va,float Vb )
        : ExpVf(Va,Vb) {}
};

divVf inline operator / (const  VecNf& Va,float Vb )
{
    return divVf( Va, Vb );
}

class maxVf:public ExpVf{
public:
    maxVf(const  VecNf& Va,float Vb )
        : ExpVf(Va,Vb) {}
};

class minVf:public ExpVf{
public:
    minVf(const  VecNf& Va,float Vb )
        : ExpVf(Va,Vb) {}
};


class Exp1{
public:
    const VecNf &V1;
    inline Exp1(const VecNf& Va )
        : V1( Va ) {}
};

class expV:public Exp1{
public:
    expV(const  VecNf& Va )
        : Exp1(Va) {}
};

class VecNf
{
private:

    int size_var;
    int size_vec;
    std::shared_ptr <float>  ptr;
    inline float *get() const
    {
        return this->ptr.get();
    }


public:
    int bulk;
    typedef float vec_t
    __attribute__ ((vector_size (sizeof(float) * 8)));
    VecNf(){}
    ~VecNf(){
        if(this->ptr.use_count() == 1)
            memory_handle(this->ptr, this->size_vec, MEMORY_OP::DEALLOC);
    }
    VecNf(int dim,const float *data);
    VecNf(int dim, float data);
    static VecNf from_vector(const std::vector<float>& d);
    static VecNf rnd(int dim);
    static VecNf zeros(int dim);
    static VecNf ones(int dim);
    static maxVf max(const VecNf& v, float f){
        return maxVf(v,f);
    }

    static inline minVf min(const VecNf& v, float f){
        return minVf(v,f);
    }

    static inline expV exp(const VecNf&v){
        return expV(v);
    }

    static float sum(const VecNf&v);
    static inline float maximun(const VecNf &v)
    {
        auto f = FLT_MIN;

        __m256 * Av = (__m256*)v.get();
        float * fAv = v.get();

        __m256 Lv{f,f,f,f,f,f,f,f};;;
        for(int i = 0; i < v.bulk; ++i) {
            Lv = _mm256_max_ps( Av[i], Lv );
        }
        union {
            __m256 tempv;
            float tempf[8];
        };

        tempv = Lv;
        float dot{f};
        for(int i = 0; i < 8; ++i) {
            dot = std::max(tempf[i], dot);
        }
        for(int i = v.bulk * 8; i < v.size_var ; i++){
            dot = std::max(fAv[i], dot);
        }
        return dot;
    }


    //static VecNf randoms();
    bool operator==(const VecNf& v)const;

    inline void operator=(const sumVf& e) const
    {
        float f = e.V2;
        vec_t Av{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)e.V1.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av + Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);
    }

    inline void operator=(const minusVf& e) const
    {
        float f = e.V2;
        vec_t Av{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)e.V1.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Bv[i] - Av;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);
    }


    VecNf inv();


    void operator-=(float f)
    {
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] -= b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }


    inline void operator+=(float f)
    {
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] += b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }

    inline void operator=(float f)
    {
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)this->get();
        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] = b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);
    }


    inline void operator*=(const VecNf &v)
    {
        vec_t *Av = (vec_t *)v.get();
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] *= Av[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    inline void operator+=(const VecNf &v)
    {
        //assert(this->size() == v.size() && "Test size in void VecNf::operator+=(const VecNf &v)");

        vec_t *Av = (vec_t *)v.get();
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] += Av[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }

    void operator-=(const VecNf &v)
    {
        vec_t *Av = (vec_t *)v.get();
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] -= Av[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);


    }
    inline void operator/=(float scalar)
    {
        auto f = (1.f/(scalar));

        vec_t *Av = (vec_t *)this->get();
        vec_t b{f,f,f,f,f,f,f,f};
        for(int i = 0; i < this->bulk; ++i) {
            Av[i] *= (b);
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }

    inline void operator/=(const VecNf &v)
    {
        vec_t *Av = (vec_t *)v.get();
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] /= (Av[i]);
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }


    void print_vector(const std::string& str = ""){
        float *v = (float *)this->get();
        std::cout << str << ": [";
        for(int i = 0; i < this->size_var; ++i) {
            std::cout << v[i] ;
            if(i+1 < this->size_var)
                std::cout << ",";
        }
        std::cout << "]\n";
    }

    inline void operator*=(float f){

        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Bv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Bv[i] *= b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);


    }
    VecNf operator/(const VecNf& v)const;





    inline  float &operator [](size_t sz)
    {
        return this->ptr.get()[sz];
    }

    inline const float &operator [](size_t sz) const
    {
        return this->ptr.get()[sz];
    }

    inline float dot(const VecNf &v)const
    {

        vec_t temp = {0};
        vec_t *Av = (__m256 *)v.get();
        vec_t *Bv = (__m256 *)this->get();

        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

        for(int i = 0; i < this->bulk; ++i) {
            temp += *Av * *Bv;

            Av++;
            Bv++;
        }

        union {
            vec_t tempv;
            float tempf[8];
        };

        tempv = temp;

        float dot = 0;

        for(int i = 0; i < 8; ++i) {
            dot += tempf[i];
        }

        return dot;
    }
    VecNf copy();
    float norm();
    inline size_t size() const
    {
        return this->size_var;
    }
    void padding(int num, float value);



    void inline operator=(const sumV& v)
    {

        vec_t *Av = (vec_t *)v.V1.get();
        vec_t *Bv = (vec_t *)v.V2.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] + Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);
    }

    void inline operator=(const minusV& v)
    {

        vec_t *Av = (vec_t *)v.V1.get();
        vec_t *Bv = (vec_t *)v.V2.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] - Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);
    }

    void inline operator=(const mulV &v)
    {

        vec_t *Av = (vec_t *)v.V1.get();
        vec_t *Bv = (vec_t *)v.V2.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] * Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    void inline operator=(const divV &v)
    {

        vec_t *Av = (vec_t *)v.V1.get();
        vec_t *Bv = (vec_t *)v.V2.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] / Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    void inline operator+=(const mulV &v)
    {

        vec_t *Av = (vec_t *)v.V1.get();
        vec_t *Bv = (vec_t *)v.V2.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] += Av[i] * Bv[i];
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    inline void operator=(const mulVf& exp)const
    {
        float f = exp.V2;
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Av = (vec_t *)exp.V1.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] * b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    inline void operator=(const divVf& exp)const
    {
        float f = exp.V2;
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Av = (vec_t *)exp.V1.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] = Av[i] * b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    inline void operator+=(const mulVf& exp)const
    {
        float f = exp.V2;
        vec_t b{f,f,f,f,f,f,f,f};
        vec_t *Av = (vec_t *)exp.V1.get();
        vec_t *Rv = (vec_t *)this->get();

        for(int i = 0; i < this->bulk; ++i) {
            Rv[i] += Av[i] * b;
        }
        std::fill(this->get() + this->size_var, this->get() + this->size_vec, 0.f);

    }
    inline void operator=(const maxVf& exp)const
    {
        float f = exp.V2;
        __m256 * Av = (__m256*)exp.V1.get();
        __m256 * Rv = (__m256*)this->get();
        float * fAv = exp.V1.get();
        float * fRv = this->get();

        vec_t Lv{f,f,f,f,f,f,f,f};
        for(int i = 0; i < this->bulk-1; ++i) {
            Rv[i] = _mm256_max_ps( Av[i], (__m256)Lv );
        }
        for(int i = (this->bulk-1) * 8; i < this->size_var ; i++){
            fRv[i] = fmaxf(fAv[i], f);
        }

    }
    inline void operator=(const minVf& exp)const
    {
        float f = exp.V2;
        __m256 * Av = (__m256*)exp.V1.get();
        __m256 * Rv = (__m256*)this->get();
        float * fAv = exp.V1.get();
        float * fRv = this->get();

        vec_t Lv{f,f,f,f,f,f,f,f};
        for(int i = 0; i < this->bulk-1; ++i) {
            Rv[i] = _mm256_min_ps( Av[i], (__m256)Lv );
        }
        for(int i = (this->bulk-1) * 8; i < this->size_var ; i++){
            fRv[i] = fminf(fAv[i], f);
        }

    }
    inline void operator=(const expV& ep)
    {
        __m256 * Av = (__m256*)ep.V1.get();
        __m256 * Rv = (__m256*)this->get();
        float * fAv = ep.V1.get();
        float * fRv = this->get();

        for(int i = 0; i < this->bulk-1; ++i) {
            Rv[i] = exp256_ps(Av[i]);
        }
        for(int i = (this->bulk-1) * 8; i < this->size_var ; i++){
            fRv[i] = std::exp(fAv[i]);
        }
    }
};



#endif // VECNF_H
