#pragma once

#include <string.h>

#include <vector>

#include <iostream>
#include <fstream>
#include <set>

template <class T>
class Datasets
{
public:
    class PLANO_UNIFORM
    {
    public: /**  YEAR ,  Population **/
        static std::vector<std::vector<T>> X()
        {

            std::vector<std::vector<T>> data;

            for (T i = -50; i < 50; i += 1)
            {
                for (T j = -50; j < 50; j += 1)
                {
                    data.push_back({i, j});
                }
            }
            return data;
        }

        static std::vector<std::vector<T>> Y() { return {}; }
    };

    class seeds_dataset
    {
    public:
        static std::vector<std::vector<T>> X()
        {
            std::ifstream file;
            // fie.exceptions(std::ifstream::badbit); // No need to check failbit
            char line[200];
            std::vector<std::vector<T>> ret;
            for (size_t i = 0; i < 2; i++)
                try
                {
                    file.open("seeds_dataset.txt");
                    if (!file.is_open())
                        throw std::runtime_error("Unable To Open dataset file");

                    while (file.getline(line, 200))
                    {
                        std::vector<float> data_line{0, 0, 0, 0, 0, 0, 0, 0};
                        char name[50];
                        auto nb_read = sscanf(line, "%f %f %f %f %f %f %f %f\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], &data_line[4], &data_line[5], &data_line[6], &data_line[7]);
                        if (nb_read == 8)
                            ret.push_back({data_line[0], data_line[1], data_line[2], data_line[3], data_line[4], data_line[5], data_line[6]});
                        // std::cout << nb_read << std::endl;
                    }

                    // for line-oriented input use file.getline(s)
                }
                catch (const std::runtime_error &e)
                {
                    std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/00236/seeds_dataset.txt");
                }

            file.close();

            return ret;
        }
        static size_t nb_class() { ; }
        static std::vector<std::vector<size_t>> Y()
        {
            ;
        }
    };
    class buddymove_holidayiq
    {
    public:
        static std::vector<std::vector<T>> X()
        {
            std::ifstream file;
            // fie.exceptions(std::ifstream::badbit); // No need to check failbit
            char line[200];
            std::vector<std::vector<T>> ret;
            for (size_t i = 0; i < 2; i++)
                try
                {
                    file.open("buddymove_holidayiq.csv");
                    if (!file.is_open())
                        throw std::runtime_error("Unable To Open dataset file");
                    file.getline(line, 200);
                    while (file.getline(line, 200))
                    {
                        std::vector<float> data_line{0, 0, 0, 0, 0, 0, 0};
                        char name[50];
                        auto nb_read = sscanf(line, "%s %f,%f,%f,%f,%f,%f,%f\n",name, &data_line[0], &data_line[1], &data_line[2], &data_line[3], &data_line[4], &data_line[5], &data_line[6]);
                        if (nb_read == 8)
                            ret.push_back({ data_line[1], data_line[2], data_line[3], data_line[4], data_line[5], data_line[6]});
                        // std::cout << nb_read << std::endl;
                    }

                    // for line-oriented input use file.getline(s)
                }
                catch (const std::runtime_error &e)
                {
                    std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/00476/buddymove_holidayiq.csv");
                }

            file.close();

            return ret;
        }
        static size_t nb_class() { ; }
        static std::vector<std::vector<size_t>> Y()
        {
            ;}
        };

        class IRIS
        {
        public:
            static std::vector<std::vector<T>> X()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::vector<T>> ret;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5)
                                ret.push_back({data_line[0], data_line[1], data_line[2], data_line[3]});
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                return ret;
            }
            static size_t nb_class() { return 3; }
            static std::vector<std::vector<size_t>> Y()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::string> ret;
                std::set<std::string> names;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5)
                            {
                                ret.push_back(name);
                                names.insert(name);
                            }
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                std::vector<std::vector<size_t>> class_ret;

                for (auto &it : ret)
                {
                    size_t class_id = std::distance(names.begin(), names.find(it));
                    class_ret.push_back({class_id});
                }

                return class_ret;
            }
        };

        class IRIS2Class
        {
        public:
            static std::vector<std::vector<T>> X()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::vector<T>> ret;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5 && (strcmp("Iris-setosa", name) == 0 || strcmp("Iris-virginica", name) == 0))
                                ret.push_back({data_line[0], data_line[1], data_line[2], data_line[3]});
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                return ret;
            }
            static size_t nb_class() { return 2; }
            static std::vector<std::vector<size_t>> Y()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::string> ret;
                std::set<std::string> names;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5 && (strcmp("Iris-setosa", name) == 0 || strcmp("Iris-virginica", name) == 0))
                            {
                                ret.push_back(name);
                                names.insert(name);
                            }
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                std::vector<std::vector<size_t>> class_ret;

                for (auto &it : ret)
                {
                    size_t class_id = std::distance(names.begin(), names.find(it));
                    class_ret.push_back({class_id});
                }

                return class_ret;
            }
        };
        class IRIS2Class2Feature
        {
        public:
            static std::vector<std::vector<T>> X()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::vector<T>> ret;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5 && (strcmp("Iris-setosa", name) == 0 || strcmp("Iris-virginica", name) == 0))
                                ret.push_back({data_line[0], data_line[1]});
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                return ret;
            }
            static size_t nb_class() { return 2; }
            static std::vector<std::vector<size_t>> Y()
            {
                std::ifstream file;
                // fie.exceptions(std::ifstream::badbit); // No need to check failbit
                char line[200];
                std::vector<std::string> ret;
                std::set<std::string> names;
                for (size_t i = 0; i < 2; i++)
                    try
                    {
                        file.open("iris.data");
                        if (!file.is_open())
                            throw std::runtime_error("Unable To Open dataset file");

                        while (file.getline(line, 200))
                        {
                            std::vector<float> data_line{0, 0, 0, 0};
                            char name[50];
                            auto nb_read = sscanf(line, "%f,%f,%f,%f,%s\n", &data_line[0], &data_line[1], &data_line[2], &data_line[3], name);
                            if (nb_read == 5 && (strcmp("Iris-setosa", name) == 0 || strcmp("Iris-virginica", name) == 0))
                            {
                                ret.push_back(name);
                                names.insert(name);
                            }
                            // std::cout << nb_read << std::endl;
                        }

                        // for line-oriented input use file.getline(s)
                    }
                    catch (const std::runtime_error &e)
                    {
                        std::system("wget https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data");
                    }

                file.close();

                std::vector<std::vector<size_t>> class_ret;

                for (auto &it : ret)
                {
                    size_t class_id = std::distance(names.begin(), names.find(it));
                    class_ret.push_back({class_id});
                }

                return class_ret;
            }
        };
        class US_CENSUS
        {
        public: /**  YEAR ,  Population **/
            static std::vector<std::vector<T>> X() { return {{1900},
                                                             {1910},
                                                             {1920},
                                                             {1930},
                                                             {1940},
                                                             {1950},
                                                             {1960},
                                                             {1970},
                                                             {1980},
                                                             {1990},
                                                             {2000}}; }

            static std::vector<std::vector<T>> Y() { return {{75.9950},
                                                             {91.9720},
                                                             {105.7110},
                                                             {123.2030},
                                                             {131.6690},
                                                             {150.6970},
                                                             {179.3230},
                                                             {203.2120},
                                                             {226.5050},
                                                             {249.6330},
                                                             {281.4220}}; }
        };

        class BOOKS_ATENDE_GRADE
        {
        public: /**  YEAR ,  Population **/
            static std::vector<std::vector<T>> X() { return {{0, 9},
                                                             {1, 15},
                                                             {0, 10},
                                                             {2, 16},
                                                             {4, 10},
                                                             {4, 20},
                                                             {1, 11},
                                                             {4, 20},
                                                             {3, 15},
                                                             {0, 15},
                                                             {2, 8},
                                                             {1, 13},
                                                             {4, 18},
                                                             {1, 10},
                                                             {0, 8},
                                                             {1, 10},
                                                             {3, 16},
                                                             {0, 11},
                                                             {1, 19},
                                                             {4, 12},
                                                             {4, 11},
                                                             {0, 19},
                                                             {2, 15},
                                                             {3, 15},
                                                             {1, 20},
                                                             {0, 6},
                                                             {3, 15},
                                                             {3, 19},
                                                             {2, 14},
                                                             {2, 13},
                                                             {3, 17},
                                                             {2, 20},
                                                             {2, 11},
                                                             {3, 20},
                                                             {4, 20},
                                                             {4, 20},
                                                             {3, 9},
                                                             {1, 8},
                                                             {2, 16},
                                                             {0, 10}}; }

            static std::vector<std::vector<T>> Y() { return {{45},
                                                             {57},
                                                             {45},
                                                             {51},
                                                             {65},
                                                             {88},
                                                             {44},
                                                             {87},
                                                             {89},
                                                             {59},
                                                             {66},
                                                             {65},
                                                             {56},
                                                             {47},
                                                             {66},
                                                             {41},
                                                             {56},
                                                             {37},
                                                             {45},
                                                             {58},
                                                             {47},
                                                             {64},
                                                             {97},
                                                             {55},
                                                             {51},
                                                             {61},
                                                             {69},
                                                             {79},
                                                             {71},
                                                             {62},
                                                             {87},
                                                             {54},
                                                             {43},
                                                             {92},
                                                             {83},
                                                             {94},
                                                             {60},
                                                             {56},
                                                             {88},
                                                             {62}}; }
        };

        class ALPS_WATER
        {
        public:
            static std::vector<std::vector<T>> X() { return {{20.79},
                                                             {20.79},
                                                             {22.40},
                                                             {22.67},
                                                             {23.15},
                                                             {23.35},
                                                             {23.89},
                                                             {23.99},
                                                             {24.02},
                                                             {24.01},
                                                             {25.14},
                                                             {26.57},
                                                             {28.49},
                                                             {27.76},
                                                             {29.04},
                                                             {29.88},
                                                             {30.06}}; }

            static std::vector<std::vector<T>> Y() { return {{194.5},
                                                             {194.3},
                                                             {197.9},
                                                             {198.4},
                                                             {199.4},
                                                             {199.9},
                                                             {200.9},
                                                             {201.1},
                                                             {201.4},
                                                             {201.3},
                                                             {203.6},
                                                             {204.6},
                                                             {209.5},
                                                             {208.6},
                                                             {210.7},
                                                             {211.9},
                                                             {212.2}}; }
        };
    };
