#include "vecnf.h"
#include <avx_mathfun.h>

#include <assert.h>
#include <random>
#include <stack>
#include <unordered_map>
#include <tuple>
#include <list>




void free_nop (void *__ptr){
    std::ignore = __ptr;
}

void memory_handle(std::shared_ptr <float>& ptr , size_t len, MEMORY_OP op){
    static std::unordered_map <size_t, std::list<std::shared_ptr <float> > > q[16];
    //static std::vector <std::shared_ptr <float> >  blocks[16];


    auto t_id = omp_get_thread_num();
    std::list<std::shared_ptr <float> >& _q = q[t_id][len];
    size_t t = _q.size();
    // printf("stack size %lu\n", t);
    if(op == MEMORY_OP::ALLOC){

        if(t > 0){
            ptr = _q.front();
            _q.pop_front();
            //printf("Get Old\n");
            return;
        }
        //printf("Alloc New\n");
        float * p = (float *)memalign(32, sizeof(float) * len);
        ptr = std::shared_ptr<float>(p, free);


//        float * p = (float *)memalign(32, sizeof(float) * len * 2);
//        blocks[t_id].push_back( std::shared_ptr<float>(p, free));
//        for(int i = 1 ; i < 2; ++i){
//            _q.push_front(std::shared_ptr<float>(p+(i*len), free_nop));
//        }
//        ptr = std::shared_ptr<float>(p, free_nop);
        return;

    }
    if(op == MEMORY_OP::DEALLOC){

        _q.push_front(ptr);

        return;

    }
}

void fillramdon(float * p , size_t n){

    std::random_device rd;  //Will be used to obtain a seed for the random number engine
    std::mt19937 gen(rd()); //Standard mersenne_twister_engine seeded with rd()
    std::uniform_real_distribution<float> dis(-1, 1);
    for(size_t i = 0 ; i < n ; i++){
        p[i] = dis(gen);
    }


}

VecNf VecNf::from_vector(const std::vector<float>& d){
    auto v = VecNf(d.size(),d.data());
    return v;
}

VecNf::VecNf(const int dim,const float* data)
{
    if(dim % 8 == 0){
        this->size_var = this->size_vec = dim;
    }else{
        this->size_vec = 8 * (dim/8 +1);
        this->size_var = dim;
    }
    this->bulk = this->size_vec/8;
    memory_handle(this->ptr, this->size_vec, MEMORY_OP::ALLOC);


    std::copy(data, data + this->size_var, this->ptr.get());
    std::fill(this->ptr.get() + this->size_var, this->ptr.get() + this->size_vec, 0.f);
}

VecNf::VecNf(int dim, float data)
{
    if((dim % 8) == 0){
        this->size_var = this->size_vec = dim;
    }else{
        this->size_vec = 8 * (dim/8 +1);
        this->size_var = dim;
    }
    this->bulk = this->size_vec/8;
    memory_handle(this->ptr, this->size_vec, MEMORY_OP::ALLOC);


    std::fill(this->ptr.get(), this->ptr.get() + this->size_var, data);
    std::fill(this->ptr.get() + this->size_var, this->ptr.get() + this->size_vec, 0.f);

}

VecNf VecNf::rnd(int dim)
{
    auto v = VecNf(dim,0.f);
    fillramdon(v.get(),v.size());
    return v;
}

VecNf VecNf::zeros(int dim)
{
    return VecNf(dim,0.f);
}

VecNf VecNf::ones(int dim)
{
    return VecNf(dim,1.f);
}







float VecNf::sum(const VecNf &v)
{
    auto f = 0.f;
    vec_t temp{f,f,f,f,f,f,f,f};


    vec_t *Av = (vec_t *)v.get();
    float * fAv = v.get();


    for(int i = 0; i < v.bulk-1; ++i) {
        temp += Av[i];
    }

    union {
        vec_t tempv;
        float tempf[8];
    };

    tempv = temp;

    float dot = 0;
    for(int i = 0; i < 8; ++i) {
        dot += tempf[i];
    }
    for(int i = (v.bulk-1) * 8; i < v.size_var ; i++){
        dot += fAv[i];
    }
    return dot;
}


bool VecNf::operator==(const VecNf& v)const
{
    if(this->size() != v.size())return false;
    float * p = this->get();
    return std::equal( p , p + this->size(),
                       v.get() );
}









VecNf VecNf::inv()
{
    VecNf ret = VecNf::zeros(this->size());
    auto f = 1.f;
    vec_t Av{f,f,f,f,f,f,f,f};;
    vec_t *Bv = (vec_t *)this->get();
    vec_t *Rv = (vec_t *)ret.get();

    for(int i = 0; i < this->bulk; ++i) {
        Rv[i] =  Av[i]  / (Bv[i] );
    }
    std::fill(ret.get() + ret.size_var, ret.get() + ret.size_vec, 0.f);

    return ret;
}




















//inline  float &VecNf::operator [](size_t sz)
//{
//    return this->ptr.get()[sz];
//}

//inline const float &VecNf::operator [](size_t sz) const
//{
//    return this->ptr.get()[sz];
//}



VecNf VecNf::copy()
{
    VecNf ret = VecNf::zeros(this->size());
    float * p = this->get();
    std::copy(p, p + this->size(), ret.get());
    return ret;
}

float VecNf::norm()
{
    return std::sqrt(this->dot(*this));
}






