#ifndef LOSSES_H
#define LOSSES_H

#include <vecnf.h>
#include <vector>

void loss_derivate(const std::vector<VecNf> &net_out, const std::vector<VecNf> &expected, std::vector<VecNf> &ret);

#endif // LOSSES_H
