#pragma once

#include <array>
#include <memory>
#include <iostream>
#include <vector>

template <class T>
class Matrix2D {
private:

    class Matrix2DProxy {
    private:
        std::shared_ptr<T> ptr;
        size_t shape1;


    public:
        size_t d0;

        Matrix2DProxy(const std::shared_ptr<T>& p, const size_t& d): shape1(d){ ptr = p; }

        // Copy constructor
        Matrix2DProxy(const Matrix2DProxy &p2)
        {
            ptr = p2.ptr;
            shape1 = p2.shape1;
            d0 = p2.d0;
        }


        T& operator[](const size_t& d1) {
            if(d1 < shape1){
                size_t index = this->d0 * this->shape1 + d1;
                return ptr.get()[index];
            }
            else{
                std::cerr << "Out of Bounds in D1; index: " << d1;
                throw std::runtime_error("Out of Bounds in D1\n");}
        }
    };
    Matrix2D (){}


    std::shared_ptr<Matrix2DProxy> proxy;



    std::shared_ptr<T> data;

public:

    static constexpr T TOL = 1e-6;


    Matrix2D (const std::array<size_t, 2>& s): shape(s) {
        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));
    }

    Matrix2D (const std::vector<std::vector <T> >& s){
        auto sz = s[0].size();
        for(size_t i = 1; i < s.size(); i++ ){
            if(sz != s[i].size()){
                throw std::runtime_error("Lines must be equal");
            }
        }
        shape = {s.size(), sz};

        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));

        for(size_t i = 0 ; i < s.size() ; i++ )
            for(size_t j = 0 ; j < sz ; j++ ){
                this->operator [](i)[j] = s[i][j];
            }


    }
    // Copy constructor
    Matrix2D(const Matrix2D &p2)
    {
        shape = p2.shape;
        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));
        std::copy(p2.data.get(), p2.data.get() +(shape[0] * shape[1]), data.get());
    }

    std::array<size_t, 2> shape; //shape[0] == nrows and shape[1] == ncols

    Matrix2DProxy& operator[](const size_t& d0) {
        if(d0 < shape[0]){
            proxy->d0 = d0;
            return *proxy;
        }
        else{
            throw std::runtime_error("Out of Bounds in D0\n");}
    }




    static Matrix2D<T> zeros(const std::array<size_t, 2>& s)
    {
        return Matrix2D<T>(s);
    }

    static Matrix2D<T> zeros(const size_t& s)
    {
        return Matrix2D<T>({s,s});
    }

    static Matrix2D<T> eye(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});
        for(size_t i = 0 ; i < s ; i++ )
            m[i][i] = 1;

        return m;
    }


    static Matrix2D<T> ones(const std::array<size_t, 2>& s)
    {
        auto m = Matrix2D<T>(s);

        for(size_t i = 0 ; i < s[0] ; i++ )
            for(size_t j = 0 ; j < s[1] ; j++ )
                m[i][j] = 1;

        return m;
    }

    static Matrix2D<T> ones(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});

        for(size_t i = 0 ; i < s ; i++ )
            for(size_t j = 0 ; j < s ; j++ )
                m[i][j] = 1;

        return m;
    }

    static Matrix2D<T> arange(const std::array<size_t, 2>& s)
    {
        auto m = Matrix2D<T>(s);

        for(size_t i = 0 ; i < s[0] ; i++ )
            for(size_t j = 0 ; j < s[1] ; j++ )
                m[i][j] = i * m.shape[0] + j;

        return m;
    }

    static Matrix2D<T> arange(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});

        for(size_t i = 0 ; i < s ; i++ )
            for(size_t j = 0 ; j < s ; j++ )
                m[i][j] = i * m.shape[0] + j;

        return m;
    }

    void print_shape(){

        std::cout << "(" << shape[0] << ", " << shape[1] <<  ")\n";

    }

    void print_matrix(){
        std::cout << "Matrix2d{\nshape: ";
        print_shape();
        std::cout << "dtype: " << typeid(T).name() << "\n";
        std::cout << "data:\n     [[";
        size_t i;
        for(i = 1 ;i < shape[0] * shape[1] ; i++){
            std::cout << std::showpos << data.get()[i-1];
            if( (i % shape[1])  == 0){
                std::cout <<"],\n      [";
            }
            else{
                std::cout <<", ";
            }
        }
        std::cout << data.get()[i-1];
        std::cout << "]]\n}\n";
    }

    Matrix2D<T> transpose()const{
        auto B = *this;
        Matrix2D<T> A = Matrix2D<T>({shape[1],shape[0]});
        for(size_t i = 0 ; i < A.shape[0] ; i++ )
            for(size_t j = 0 ; j < A.shape[1] ; j++ )
                A[i][j] = B[j][i];
        return A;
    }

    Matrix2D<T> dot( Matrix2D<T>& B)const{

        auto A = *this;

        if(A.shape[1] != B.shape[0])
            throw std::runtime_error("For A.dot(B)\nDimension 1 of A  must equal Dimention 0 of B");

        auto C = Matrix2D<T>({A.shape[0],B.shape[1]});

        for(size_t i = 0 ; i < C.shape[0] ; i++ )
            for(size_t j = 0 ; j < C.shape[1] ; j++ ){
                T acc = 0;
                for(size_t k = 0 ; k < B.shape[0] ; k++ ){
                    T a = A[i][k];
                    T b = B[k][j];
                    acc = acc + (a * b);
                }

                C[i][j] = acc;
            }
        return C;
    }
    bool isSquare()const{
        return shape[0] == shape[1];
    }

    Matrix2D<T> coFactor(size_t p, size_t q)const
    {
        auto n = shape[0];
        auto temp  = Matrix2D<T>::zeros(shape[0] -1);
        auto A = *this;
        size_t i = 0, j = 0;

        // Looping for each element of the matrix
        for (size_t row = 0; row < n; row++)
        {
            for (size_t col = 0; col < n; col++)
            {
                //  Copying into temporary matrix only those element
                //  which are not in given row and column
                if (row != p && col != q)
                {
                    temp[i][j++] = A[row][col];

                    // Row is filled, so increase row index and
                    // reset col index
                    if (j == n - 1)
                    {
                        j = 0;
                        i++;
                    }
                }
            }
        }
        return temp;
    }


    T det()const{
        auto A = *this;
        T D = 0; // Initialize result
        auto n = shape[0];

        //  Base case : if matrix contains single element
        if (n == 1)
            return A[0][0];

        int sign = 1;  // To store sign multiplier

        // Iterate for each element of first row
        for (size_t f = 0; f < n; f++)
        {
            D += sign * A[0][f] * A.coFactor(0,f).det();

            // terms are to be added with alternate sign
            sign = -sign;
        }

        return D;

    }

    Matrix2D<T> adjoint()const
    {
        auto A = *this;
        auto n = shape[0];
        auto adj = Matrix2D<T>::zeros(n);

        if (n == 1)
        {
            return Matrix2D<T>::ones(1);
        }


        int sign = 1;

        for (size_t i=0; i<n; i++)
        {
            for (size_t j=0; j<n; j++)
            {
                // sign of adj[j][i] positive if sum of row
                // and column indexes is even.
                sign = ((i+j)%2==0)? 1: -1;

                // Interchanging rows and columns to get the
                // transpose of the cofactor matrix
                adj[j][i] = (sign)*(A.coFactor(i,j).det());
            }
        }
        return adj;
    }


    Matrix2D<T> inv()const{

        if(!this->isSquare())
            throw std::runtime_error("For A.inv()\nA must be a square matrix");

        auto A = *this;
        auto n = shape[0];
        auto inverse = Matrix2D<T>::zeros(n);

        T det = A.det();
        if (det == 0)
        {
            throw std::runtime_error("Singular matrix, can't find its inverse");
        }


        auto adj =  A.adjoint();

        // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
        for (size_t i=0; i<n; i++)
            for (size_t j=0; j<n; j++)
                inverse[i][j] = adj[i][j]/det;

        return inverse;
    }

    Matrix2D<T> pseudo_inv()const{
        try{
            if(isSquare()){
                return this->inv();
            }
        }catch(const std::runtime_error& e){
            ;
        }
        if(shape[0] >= shape[1]){
            auto A = *this;
            auto AT = A.transpose();
            return AT.dot(A).inv().dot(AT);

        }else{
            auto A = *this;
            auto AT = A.transpose();
            auto ATA_1 = AT.dot(A).inv();
            return AT.dot(ATA_1);
        }
    }

    bool almostClose(const Matrix2D<T>& B,T tol = TOL){
        for(size_t i = 0; i < shape[0]*shape[1]; i++)
            if(std::abs(data.get()[i] - B.data.get()[i]) > tol){
                return false;
            }
        return true;
    }

};



