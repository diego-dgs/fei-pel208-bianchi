#include "matrix2d.h"
#include "least_squares.h"
#include "datasets.h"


int main(int argc, char *argv[])
{
    {
        //USA CENSUS
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::US_CENSUS::X());

        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::US_CENSUS::Y());

        LeastSquares<long double> model;

        model.fit(X,y, LeastSquares<long double>::NORMAL );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";



        model.fit(X,y, LeastSquares<long double>::QUADRATIC );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.fit(X,y, LeastSquares<long double>::ROBUST );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.plot("USA CENSUS");


    }

    {
        //BOOKS_ATENDE_GRADE
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::BOOKS_ATENDE_GRADE::X());

        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::BOOKS_ATENDE_GRADE::Y());

        LeastSquares<long double> model;

        model.fit(X,y, LeastSquares<long double>::NORMAL );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.fit(X,y, LeastSquares<long double>::QUADRATIC );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.fit(X,y, LeastSquares<long double>::ROBUST );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.plot("BOOKS ATENDE GRADE");

    }
    {
        //USA ALPS_WATER
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::ALPS_WATER::X());

        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::ALPS_WATER::Y());

        LeastSquares<long double> model;

        model.fit(X,y, LeastSquares<long double>::NORMAL );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.fit(X,y, LeastSquares<long double>::QUADRATIC );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";

        model.fit(X,y, LeastSquares<long double>::ROBUST );
        model.print_model();
        std::cout << "Error: "<< model.score(X,y) << ";\n";
        model.plot("ALPS_WATER");

    }


    return 0;
}




void unit_tests(){
    //    auto m = Matrix2D<long double>({2,2});
    {

        auto m = Matrix2D<long double>::zeros({2,2});
        m.print_shape();
        m.print_matrix();

        std::cout << '\n';

        auto eye = Matrix2D<long double>::eye(3);
        eye.print_shape();
        eye.print_matrix();

        std::cout << '\n';
        auto ones = Matrix2D<long double>::ones(3);

        auto C = ones.dot(eye);
        C.print_shape();
        C.print_matrix();

        auto A = Matrix2D<long double>::arange(3);
        auto B = Matrix2D<long double>::arange(3);

        auto D = A.dot(B);

        D.print_matrix();

        D = D.transpose();

        D.print_matrix();

        D.coFactor(0,0).print_matrix();
        D.coFactor(0,1).print_matrix();
        D.coFactor(0,2).print_matrix();

        std::cout << D.det() << "\n";
        auto one = Matrix2D<long double>::ones(1);

        one.print_matrix();
    }
    Matrix2D<long double> A =   Matrix2D<long double>({{5,  -2,  2 , 7},
                                                       {1,   0,  0,  3},
                                                       {-3,  1,  5,  0},
                                                       {3,  -1, -9,  4}});

    A.print_matrix();

    std::cout << A.det() << "\n";

    A.adjoint().print_matrix();


    A.inv().print_matrix();

    A.pseudo_inv().pseudo_inv().print_matrix();

    std::cout << A.pseudo_inv().pseudo_inv().almostClose(A) << "\n";
}
