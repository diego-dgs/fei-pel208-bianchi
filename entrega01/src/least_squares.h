#pragma once

#include "matrix2d.h"

#include "gnuplot.h"


#include <iostream>
#include <fstream>
#include <sstream>


template <class T>
class LeastSquares{
public:
    typedef enum{
        NORMAL,
        QUADRATIC,
        ROBUST
    }LeastSquaresMode;




    std::array<T,3> intercept_;
    std::array<bool,3> isFitted;
    std::array<Matrix2D<T>,3> coef_;
    Matrix2D<T> W_;
    Matrix2D<T> residuals_;
    Matrix2D<T> B_;
    Matrix2D<T> X_;
    Matrix2D<T> y_;
    Matrix2D<T> OX;
    Matrix2D<T> Oy;
    LeastSquaresMode mode;


    // AbstractLinearModel interface
public:

    LeastSquares():isFitted({0,0,0}), coef_({Matrix2D<T>::zeros({1,1}),Matrix2D<T>::zeros({1,1}),Matrix2D<T>::zeros({1,1})}), W_({1,1}), residuals_({1,1}), B_({1,1}), X_({1,1}), y_({1,1}), OX({1,1}), Oy({1,1}){

    }

    virtual ~LeastSquares(){

    }

    void fit(const Matrix2D<T> &X, const Matrix2D<T> &y, const LeastSquaresMode& mode = LeastSquaresMode::NORMAL){
        // (X^T x W*X) x X^T x W*y
        this->mode = mode;
        OX = X;
        Oy = y;
        if(X.shape[0] != y.shape[0])
            throw std::runtime_error("Dim 0 must have the same size for X and y;");

        if(y.shape[1] > 1)
            throw std::runtime_error("Just Single variate problems");

        prepare_input(X,y);


        B_ = X_.pseudo_inv().dot(y_);


        intercept_[this->mode] = B_[0][0];
        coef_[this->mode] = Matrix2D<T>::zeros({B_.shape[0]-1, B_.shape[1]});
        for(size_t  i = 1; i < B_.shape[0]; i++)
            coef_[this->mode][i-1][0] = B_[i][0];

        residuals_ = X_.dot(B_);
        for(size_t i = 0; i < residuals_.shape[0]; i++ ){
            auto diff = residuals_[i][0] - y_[i][0];
            residuals_[i][0] = diff;
        }
        if(this->mode == ROBUST){
            auto W = Matrix2D<T>::zeros({X_.shape[0], X_.shape[0]});

            for(size_t i = 0; i < W.shape[0]; i++)
                W[i][i] = 1.0/(0.00001 + std::abs(residuals_[i][0]));

//            B = (X^T*W*X)^-1 * X^T*W*Y
            auto WX = W.dot(X_);
            auto XT = X_.transpose();
            auto Wy = W.dot(y_);
            auto XTy = XT.dot(Wy);
            B_ = XT.dot(WX).inv().dot(XTy);

            intercept_[this->mode] = B_[0][0];
            coef_ [this->mode]= Matrix2D<T>::zeros({B_.shape[0]-1, B_.shape[1]});
            for(size_t  i = 1; i < B_.shape[0]; i++)
                coef_[this->mode][i-1][0] = B_[i][0];

            residuals_ = X_.dot(B_);
            for(size_t i = 0; i < residuals_.shape[0]; i++ ){
                auto diff = residuals_[i][0] - y_[i][0];
                residuals_[i][0] = diff;
            }
        }

        isFitted[this->mode] =true;
    }

    void prepare_input(const Matrix2D<T> &X, const Matrix2D<T> &y){
        auto new_shape1 = 1 + X.shape[1] + X.shape[1] * (this->mode == QUADRATIC);

        X_ = Matrix2D<T>::ones({X.shape[0], new_shape1});

        auto A = X;
        y_ = y;

        for(size_t i = 0; i < X.shape[0]; i++)
            for(size_t j = 1; j < X.shape[1] + 1; j++)
                X_[i][j] = A[i][j-1];

        if(this->mode == QUADRATIC){
            for(size_t i = 0; i < X.shape[0]; i++)
                for(size_t j = X.shape[1]+1; j < X_.shape[1]; j++){
                    size_t idx = j -X.shape[1]-1;
                    X_[i][j] = (A[i][idx] * A[i][idx]);
                }

        }

    }
    void prepare_input(const Matrix2D<T> &X){
        auto new_shape1 = 1 + X.shape[1] + X.shape[1] * (this->mode == QUADRATIC);

        X_ = Matrix2D<T>::ones({X.shape[0], new_shape1});

        auto A = X;

        for(size_t i = 0; i < X.shape[0]; i++)
            for(size_t j = 1; j < X.shape[1] + 1; j++)
                X_[i][j] = A[i][j-1];

        if(this->mode == QUADRATIC){
            for(size_t i = 0; i < X.shape[0]; i++)
                for(size_t j = X.shape[1]+1; j < X_.shape[1]; j++){
                    size_t idx = j -X.shape[1]-1;
                    X_[i][j] = (A[i][idx] * A[i][idx]);
                }

        }

    }

    T score(const Matrix2D<T> &X, const Matrix2D<T> &y){
        if(X.shape[0] != y.shape[0])
            throw std::runtime_error("Dim 0 must have the same size for X and y;");

        if(y.shape[1] > 1)
            throw std::runtime_error("Just Single variate problems");

        auto ori_X_ = X_;
        auto ori_y_ = y_;

        prepare_input(X,y);


        residuals_ = X_.dot(B_);
        for(size_t i = 0; i < residuals_.shape[0]; i++ ){
            auto diff = residuals_[i][0] - y_[i][0];
            residuals_[i][0] = diff;
        }

        T acc = 0;
        for(size_t i = 0; i < residuals_.shape[0]; i++ ){

            acc += residuals_[i][0] * residuals_[i][0];
        }
        X_ = ori_X_;
        y_ = ori_y_;

        return acc;

    }
    Matrix2D<T> predict(const Matrix2D<T> &X){

        prepare_input(X);


        return X_.dot(B_);

    }

    void print_model(){
        if(this->isFitted[this->mode]){
             std::cout << "\n\n\n\nX:\n";
            if(this->mode == NORMAL){
                std::cout << "NORMAL mode\n";
            }else
            if(this->mode == QUADRATIC){
                std::cout << "QUADRATIC mode\n";
            }
            else
            if(this->mode == ROBUST){
                std::cout << "ROBUST mode\n";
            }
            std::cout << "----------------------------------------------\n";
            std::cout << "\nX:\n";
            X_.print_matrix();
            std::cout << "----------------------------------------------\n";
            std::cout << "\ny:\n";
            y_.print_matrix();
            std::cout << "----------------------------------------------\n";
            std::cout << "\nIntercept: "<< this->intercept_[this->mode] << ";\n";
            std::cout << "----------------------------------------------\n";
            std::cout << "\nCoeficients:\n";
            coef_[this->mode].print_matrix();
            std::cout << "----------------------------------------------\n\n\n\n";
        }else{
            std::cout << "Model Isn't fitted Yet!\n";
        }
    }

    void plot(const std::string& dataset_name){

        std::ofstream myfile;
        myfile.open ("/tmp/least_square.dat");
        for(size_t i = 0; i < OX.shape[0]; i++){
            for(size_t j = 0; j < OX.shape[1]; j++)
                myfile << OX[i][j] << "\t";
            myfile << Oy[i][0] << "\n";
        }
        myfile.close();
        GnuplotPipe gp;

        std::stringstream ss;
        if(OX.shape[1] == 1){
            ss << "plot \"/tmp/least_square.dat\" using 1:2 with points title \"" << dataset_name <<  "\", ";

            ss << intercept_[NORMAL] << " " << std::showpos << coef_[NORMAL][0][0] << "*x title \"NORMAL\", ";


            ss << intercept_[QUADRATIC] << " " << std::showpos << coef_[QUADRATIC][0][0] << "*x " << coef_[QUADRATIC][1][0] << "*x**2 title \"QUADRATIC\", ";


            ss << intercept_[ROBUST] << " " << std::showpos << coef_[ROBUST][0][0] << "*x  title \"ROBUST\" ";

            gp.sendLine(ss.str());
        }

        if(OX.shape[1] == 2){
            ss << "splot \"/tmp/least_square.dat\" using 1:2:3 with points title \"" << dataset_name <<  "\", ";

            ss << intercept_[NORMAL] << " " << std::showpos << coef_[NORMAL][0][0] << "*x " << coef_[NORMAL][1][0] << "*y  title \"NORMAL\", ";


            ss << intercept_[QUADRATIC] << " " << std::showpos << coef_[QUADRATIC][0][0] << "*x " << coef_[QUADRATIC][1][0] << "*y " << coef_[QUADRATIC][2][0] << "*x**2 " << coef_[QUADRATIC][3][0] << "*y**2 title \"QUADRATIC\", ";


            ss << intercept_[ROBUST] << " " << std::showpos << coef_[ROBUST][0][0] << "*x " << coef_[ROBUST][1][0] << "*y  title \"ROBUST\" ";

            gp.sendLine(ss.str());
        }
    }
};

//GnuplotPipe gp;
//gp.sendLine("plot  (1 2 3 4) (1 4 9 16)");
