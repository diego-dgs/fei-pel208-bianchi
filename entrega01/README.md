# Install Dependencies

```
$ sudo apt install build-essential cmake gnuplot
```

# Build

```
$ cd fei-pel208-bianchi/entrega01/
$ mkdir build
$ cd build
$ cmake .. 
$ make -j
```

# Run

```
$ cd fei-pel208-bianchi/entrega01/bin
$ ./mmq_solver
```