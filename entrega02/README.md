# Install Dependencies

```
$ sudo apt install build-essential cmake gnuplot
```

# Build

```
$ cd fei-pel208-bianchi/entrega02/
$ mkdir build
$ cd build
$ cmake .. 
$ make -j
```

# Run

```
$ cd fei-pel208-bianchi/entrega02/bin
$ ./pca_solver
```