#include "matrix2d.h"
#include "least_squares.h"
#include "datasets.h"
#include "pca.h"

void unit_tests();

int main(int argc, char *argv[])
{
    {
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::US_CENSUS::X());
        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::US_CENSUS::Y());

        auto data = Matrix2D<long double>::zeros({X.shape[0], X.shape[1]+1});
        for(size_t i = 0; i < data.shape[0]; i++){
            size_t j;
            for( j = 0; j < X.shape[1]; j++){
                data[i][j] = X[i][j];
            }
            data[i][j] = y[i][0];
        }
        {
            PCA<long double> pca = PCA<long double>(data, 2);


            LeastSquares<long double> model;
            model.fit(X,y, LeastSquares<long double>::ROBUST );

            pca.plotCompareLeastSquare(data, model, "::US-CENSUS:: PCA x LeastSquares");
            pca.plotPrincipalComponentMeanData("::US-CENSUS:: PCA");
            pca.plotPrincipalComponentOriginalData("::US-CENSUS:: PCA");

            auto tdata = pca.transform(data);

            auto inv_data = pca.inverse_transform(tdata);
            pca.plotPoints(data, tdata, "::US-CENSUS:: PCA Transform");
            pca.plotPoints(data, inv_data, "::US-CENSUS:: PCA Inverse Transform");
        }
        {
            PCA<long double> pca = PCA<long double>(data, 1);

            auto tdata = pca.transform(data);

            tdata.print_matrix();

            auto inv_data = pca.inverse_transform(tdata);
//            pca.plotPoints(data, tdata, "PCA Transform Dimention Reduction");
            pca.plotPoints(data, inv_data, "::US-CENSUS:: PCA Inverse Transform Dimention Reduction");
        }
    }

    {
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::ALPS_WATER::X());
        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::ALPS_WATER::Y());

        auto data = Matrix2D<long double>::zeros({X.shape[0], X.shape[1]+1});
        for(size_t i = 0; i < data.shape[0]; i++){
            size_t j;
            for( j = 0; j < X.shape[1]; j++){
                data[i][j] = X[i][j];
            }
            data[i][j] = y[i][0];
        }
        {
            PCA<long double> pca = PCA<long double>(data, 2);


            LeastSquares<long double> model;
            model.fit(X,y, LeastSquares<long double>::ROBUST );

            pca.plotCompareLeastSquare(data, model, "::ALPS\\_WATER:: PCA x LeastSquares");
//            pca.plotPrincipalComponentMeanData();
            pca.plotPrincipalComponentOriginalData("::ALPS\\_WATER:: PCA");

            auto tdata = pca.transform(data);

            auto inv_data = pca.inverse_transform(tdata);
            pca.plotPoints(data, tdata, "::ALPS\\_WATER:: PCA Transform");
            pca.plotPoints(data, inv_data, "::ALPS\\_WATER:: PCA Inverse Transform");
        }
        {
            PCA<long double> pca = PCA<long double>(data, 1);

            auto tdata = pca.transform(data);

            tdata.print_matrix();

            auto inv_data = pca.inverse_transform(tdata);
//            pca.plotPoints(data, tdata, "PCA Transform Dimention Reduction");
            pca.plotPoints(data, inv_data, "::ALPS\\_WATER:: PCA Inverse Transform Dimention Reduction");
        }
    }
    {
        Matrix2D<long double> X =   Matrix2D<long double>(Datasets<long double>::BOOKS_ATENDE_GRADE::X());
        Matrix2D<long double> y =   Matrix2D<long double>(Datasets<long double>::BOOKS_ATENDE_GRADE::Y());

        auto data = Matrix2D<long double>::zeros({X.shape[0], X.shape[1]+1});
        for(size_t i = 0; i < data.shape[0]; i++){
            size_t j;
            for( j = 0; j < X.shape[1]; j++){
                data[i][j] = X[i][j];
            }
            data[i][j] = y[i][0];
        }
        {
            PCA<long double> pca = PCA<long double>(data, 3);


//            LeastSquares<long double> model;
//            model.fit(X,y, LeastSquares<long double>::ROBUST );

//            pca.plotCompareLeastSquare(data, model, "::ALPS\\_WATER:: PCA x LeastSquares");
////            pca.plotPrincipalComponentMeanData();
//            pca.plotPrincipalComponentOriginalData("::ALPS\\_WATER:: PCA");

            auto tdata = pca.transform(data);

            auto inv_data = pca.inverse_transform(tdata);
            pca.plotPoints3d(data, tdata, "::BOOKS-ATENDE-GRADE:: PCA Transform");
            pca.plotPoints3d(data, inv_data, "::BOOKS-ATENDE-GRADE:: PCA Inverse Transform");
        }
        {
            PCA<long double> pca = PCA<long double>(data, 2);


//            LeastSquares<long double> model;
//            model.fit(X,y, LeastSquares<long double>::ROBUST );

//            pca.plotCompareLeastSquare(data, model, "::ALPS\\_WATER:: PCA x LeastSquares");
////            pca.plotPrincipalComponentMeanData();
//            pca.plotPrincipalComponentOriginalData("::ALPS\\_WATER:: PCA");

            auto tdata = pca.transform(data);

            auto inv_data = pca.inverse_transform(tdata);
//            pca.plotPoints3d(data, tdata, "::BOOKS-ATENDE-GRADE:: PCA Transform");
            pca.plotPoints3d(data, inv_data, "::BOOKS-ATENDE-GRADE:: PCA Inverse Transform With Reduction");
        }
    }

    return 0;
}


void jacobi_unit_test(){
    Matrix2D<long double> A =   Matrix2D<long double>({{ -893. ,   929. ,  1354.5},
                                                        {  929. ,  1976. ,  1358.5},
                                                        { 1354.5,  1358.5, -1578. }});
    Matrix2D<long double> Evectors({1,1});
    Matrix2D<long double> Evalues({1,1});
    std::cout << A.symmetricMatrixEigenVectorsAndValues(Evectors, Evalues)<< "\n";

    Evalues.print_matrix();
    Evectors.print_matrix();

    long double value = Evalues[0][0];
    Matrix2D<long double> vector({3,1});
    for(size_t i = 0; i < 3; i++){
       vector[i][0] = Evectors[i][0];
    }
    A.print_matrix();
    vector.print_matrix();
    A.dot(vector).print_matrix();
    (value * vector).print_matrix();

    std::cout << A.dot(vector).almostClose(value * vector, 1e-7) << "\n";

}

void unit_tests(){
    //    auto m = Matrix2D<long double>({2,2});
    {

        auto m = Matrix2D<long double>::zeros({2,2});
        m.print_shape();
        m.print_matrix();

        std::cout << '\n';

        auto eye = Matrix2D<long double>::eye(3);
        eye.print_shape();
        eye.print_matrix();

        std::cout << '\n';
        auto ones = Matrix2D<long double>::ones(3);

        auto C = ones.dot(eye);
        C.print_shape();
        C.print_matrix();

        auto A = Matrix2D<long double>::arange(3);
        auto B = Matrix2D<long double>::arange(3);

        auto D = A.dot(B);

        D.print_matrix();

        D = D.transpose();

        D.print_matrix();

        D.coFactor(0,0).print_matrix();
        D.coFactor(0,1).print_matrix();
        D.coFactor(0,2).print_matrix();

        std::cout << D.det() << "\n";
        auto one = Matrix2D<long double>::ones(1);

        one.print_matrix();
    }
    Matrix2D<long double> A =   Matrix2D<long double>({{5,  -2,  2 , 7},
                                                       {1,   0,  0,  3},
                                                       {-3,  1,  5,  0},
                                                       {3,  -1, -9,  4}});

    A.print_matrix();

    std::cout << A.det() << "\n";

    A.adjoint().print_matrix();


    A.inv().print_matrix();

    A.pseudo_inv().pseudo_inv().print_matrix();

    std::cout << A.pseudo_inv().pseudo_inv().almostClose(A) << "\n";
}


