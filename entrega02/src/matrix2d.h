#pragma once

#include <array>
#include <memory>
#include <iostream>
#include <vector>


#include <math.h>       /* sqrt */

template <class T>
class Matrix2D {
private:

    class Matrix2DProxy {
    private:
        std::shared_ptr<T> ptr;
        size_t shape1;


    public:
        size_t d0;

        Matrix2DProxy(const std::shared_ptr<T>& p, const size_t& d): shape1(d){ ptr = p; }

        // Copy constructor
        Matrix2DProxy(const Matrix2DProxy &p2)
        {
            ptr = p2.ptr;
            shape1 = p2.shape1;
            d0 = p2.d0;
        }


        T& operator[](const size_t& d1) {
            if(d1 < shape1){
                size_t index = this->d0 * this->shape1 + d1;
                return ptr.get()[index];
            }
            else{
                std::cerr << "Out of Bounds in D1; index: " << d1;
                throw std::runtime_error("Out of Bounds in D1\n");}
        }
    };
    Matrix2D (){}


    std::shared_ptr<Matrix2DProxy> proxy;



    std::shared_ptr<T> data;

public:

    static constexpr T TOL = 1e-8;


    Matrix2D (const std::array<size_t, 2>& s): shape(s) {
        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));
    }

    Matrix2D (const std::vector<std::vector <T> >& s){
        auto sz = s[0].size();
        for(size_t i = 1; i < s.size(); i++ ){
            if(sz != s[i].size()){
                throw std::runtime_error("Lines must be equal");
            }
        }
        shape = {s.size(), sz};

        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));

        for(size_t i = 0 ; i < s.size() ; i++ )
            for(size_t j = 0 ; j < sz ; j++ ){
                this->operator [](i)[j] = s[i][j];
            }


    }
    // Copy constructor
    Matrix2D(const Matrix2D &p2)
    {
        shape = p2.shape;
        data.reset(new T[shape[0] * shape[1]]());
        proxy.reset( new Matrix2DProxy(data, shape[1]));
        std::copy(p2.data.get(), p2.data.get() +(shape[0] * shape[1]), data.get());
    }

    std::array<size_t, 2> shape; //shape[0] == nrows and shape[1] == ncols

    Matrix2DProxy& operator[](const size_t& d0) {
        if(d0 < shape[0]){
            proxy->d0 = d0;
            return *proxy;
        }
        else{
            throw std::runtime_error("Out of Bounds in D0\n");}
    }



    Matrix2D<T> operator*(const T& n) const{
        auto A = *this;
        T * ptr = A.data.get();
        size_t sz = shape[0] * shape[1];
        for(size_t i = 0; i< sz; i++){
            ptr[i] = ptr[i] * n;
        }
        return A;
    }

    Matrix2D<T> operator-( Matrix2D<T>& B) const{
        auto A = *this;
        if(B.shape[0] == 1 && A.shape[1] == B.shape[1]){
            auto ret = Matrix2D<T>(A.shape);
            for(size_t i = 0 ; i < A.shape[0] ; i++ )
                for(size_t j = 0 ; j < A.shape[1] ; j++ )
                    ret[i][j] = A[i][j] - B[0][j];
            return ret;
        }
        throw std::runtime_error("This operator is not prepared yet to perform this kind of operation\n");
    }

    Matrix2D<T> operator+( Matrix2D<T>& B) const{
        auto A = *this;
        if(B.shape[0] == 1 && A.shape[1] == B.shape[1]){
            auto ret = Matrix2D<T>(A.shape);
            for(size_t i = 0 ; i < A.shape[0] ; i++ )
                for(size_t j = 0 ; j < A.shape[1] ; j++ )
                    ret[i][j] = A[i][j] + B[0][j];
            return ret;
        }
        throw std::runtime_error("This operator is not prepared yet to perform this kind of operation\n");
    }


    static Matrix2D<T> zeros(const std::array<size_t, 2>& s)
    {
        return Matrix2D<T>(s);
    }

    static Matrix2D<T> zeros(const size_t& s)
    {
        return Matrix2D<T>({s,s});
    }

    static Matrix2D<T> eye(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});
        for(size_t i = 0 ; i < s ; i++ )
            m[i][i] = 1;

        return m;
    }


    static Matrix2D<T> ones(const std::array<size_t, 2>& s)
    {
        auto m = Matrix2D<T>(s);

        for(size_t i = 0 ; i < s[0] ; i++ )
            for(size_t j = 0 ; j < s[1] ; j++ )
                m[i][j] = 1;

        return m;
    }

    static Matrix2D<T> ones(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});

        for(size_t i = 0 ; i < s ; i++ )
            for(size_t j = 0 ; j < s ; j++ )
                m[i][j] = 1;

        return m;
    }

    static Matrix2D<T> arange(const std::array<size_t, 2>& s)
    {
        auto m = Matrix2D<T>(s);

        for(size_t i = 0 ; i < s[0] ; i++ )
            for(size_t j = 0 ; j < s[1] ; j++ )
                m[i][j] = i * m.shape[0] + j;

        return m;
    }

    static Matrix2D<T> arange(const size_t& s)
    {
        auto m = Matrix2D<T>({s,s});

        for(size_t i = 0 ; i < s ; i++ )
            for(size_t j = 0 ; j < s ; j++ )
                m[i][j] = i * m.shape[0] + j;

        return m;
    }

    void print_shape(){

        std::cout << "(" << shape[0] << ", " << shape[1] <<  ")\n";

    }

    void print_matrix(){
        std::cout << "Matrix2d{\nshape: ";
        print_shape();
        std::cout << "dtype: " << typeid(T).name() << "\n";
        std::cout << "data:\n     [[";
        size_t i;
        for(i = 1 ;i < shape[0] * shape[1] ; i++){
            std::cout << std::showpos << data.get()[i-1];
            if( (i % shape[1])  == 0){
                std::cout <<"],\n      [";
            }
            else{
                std::cout <<", ";
            }
        }
        std::cout << data.get()[i-1];
        std::cout << "]]\n}\n";
    }

    Matrix2D<T> transpose()const{
        auto B = *this;
        Matrix2D<T> A = Matrix2D<T>({shape[1],shape[0]});
        for(size_t i = 0 ; i < A.shape[0] ; i++ )
            for(size_t j = 0 ; j < A.shape[1] ; j++ )
                A[i][j] = B[j][i];
        return A;
    }

    Matrix2D<T> dot( Matrix2D<T>& B)const{

        auto A = *this;

        if(A.shape[1] != B.shape[0])
            throw std::runtime_error("For A.dot(B)\nDimension 1 of A  must equal Dimention 0 of B");

        auto C = Matrix2D<T>({A.shape[0],B.shape[1]});

        for(size_t i = 0 ; i < C.shape[0] ; i++ )
            for(size_t j = 0 ; j < C.shape[1] ; j++ ){
                T acc = 0;
                for(size_t k = 0 ; k < B.shape[0] ; k++ ){
                    T a = A[i][k];
                    T b = B[k][j];
                    acc = acc + (a * b);
                }

                C[i][j] = acc;
            }
        return C;
    }
    bool isSquare()const{
        return shape[0] == shape[1];
    }

    Matrix2D<T> coFactor(size_t p, size_t q)const
    {
        auto n = shape[0];
        auto temp  = Matrix2D<T>::zeros(shape[0] -1);
        auto A = *this;
        size_t i = 0, j = 0;

        // Looping for each element of the matrix
        for (size_t row = 0; row < n; row++)
        {
            for (size_t col = 0; col < n; col++)
            {
                //  Copying into temporary matrix only those element
                //  which are not in given row and column
                if (row != p && col != q)
                {
                    temp[i][j++] = A[row][col];

                    // Row is filled, so increase row index and
                    // reset col index
                    if (j == n - 1)
                    {
                        j = 0;
                        i++;
                    }
                }
            }
        }
        return temp;
    }


    T det()const{
        auto A = *this;
        T D = 0; // Initialize result
        auto n = shape[0];

        //  Base case : if matrix contains single element
        if (n == 1)
            return A[0][0];

        int sign = 1;  // To store sign multiplier

        // Iterate for each element of first row
        for (size_t f = 0; f < n; f++)
        {
            D += sign * A[0][f] * A.coFactor(0,f).det();

            // terms are to be added with alternate sign
            sign = -sign;
        }

        return D;

    }

    Matrix2D<T> adjoint()const
    {
        auto A = *this;
        auto n = shape[0];
        auto adj = Matrix2D<T>::zeros(n);

        if (n == 1)
        {
            return Matrix2D<T>::ones(1);
        }


        int sign = 1;

        for (size_t i=0; i<n; i++)
        {
            for (size_t j=0; j<n; j++)
            {
                // sign of adj[j][i] positive if sum of row
                // and column indexes is even.
                sign = ((i+j)%2==0)? 1: -1;

                // Interchanging rows and columns to get the
                // transpose of the cofactor matrix
                adj[j][i] = (sign)*(A.coFactor(i,j).det());
            }
        }
        return adj;
    }


    Matrix2D<T> inv()const{

        if(!this->isSquare())
            throw std::runtime_error("For A.inv()\nA must be a square matrix");

        auto A = *this;
        auto n = shape[0];
        auto inverse = Matrix2D<T>::zeros(n);

        T det = A.det();
        if (det == 0)
        {
            throw std::runtime_error("Singular matrix, can't find its inverse");
        }


        auto adj =  A.adjoint();

        // Find Inverse using formula "inverse(A) = adj(A)/det(A)"
        for (size_t i=0; i<n; i++)
            for (size_t j=0; j<n; j++)
                inverse[i][j] = adj[i][j]/det;

        return inverse;
    }


    Matrix2D<T> mean(const size_t axis = -1)const{
        auto A = *this;
        if(axis == 0){
            auto ret = Matrix2D<T>::zeros({1,shape[1]});
            for(size_t i = 0; i < shape[0]; i++){
                for(size_t j = 0; j < shape[1]; j++){
                    ret[0][j] = ret[0][j]+ A[i][j];
                }
            }
            for(size_t j = 0; j < shape[1]; j++){
                ret[0][j] = ret[0][j]/ (T) shape[0];
            }
            return ret;
        }
        if(axis == 1){
            auto ret = Matrix2D<T>::zeros({shape[0],1});
            for(size_t i = 0; i < shape[1]; i++){
                for(size_t j = 0; j < shape[0]; j++){
                    ret[j][0] = ret[j][0]+ A[j][i];
                }
            }
            for(size_t j = 0; j < shape[0]; j++){
                ret[j][0] = ret[j][0]/ (T) shape[1];
            }
            return ret;

        }
        auto ret = Matrix2D<T>::zeros({1,1});
        for(size_t i = 0; i < shape[0]; i++){
            for(size_t j = 0; j < shape[1]; j++){
                ret[0][0] = ret[0][0]+ A[i][j];
            }
        }
        ret[0][0] = ret[0][0] / static_cast<T>(shape[0]* shape[1]);
        return ret;

    }

    Matrix2D<T> pseudo_inv()const{
        try{
            if(isSquare()){
                return this->inv();
            }
        }catch(const std::runtime_error& e){
            ;
        }
        if(shape[0] >= shape[1]){
            auto A = *this;
            auto AT = A.transpose();
            return AT.dot(A).inv().dot(AT);

        }else{
            auto A = *this;
            auto AT = A.transpose();
            auto ATA_1 = A.dot(AT).inv();
            return AT.dot(ATA_1);
        }
    }

    bool almostClose(const Matrix2D<T>& B,T tol = TOL){

        T * a = data.get();
        T * b = B.data.get();


        for(size_t i = 0; i < shape[0]*shape[1]; i++)
            if(std::abs(a[i] - b[i]) > tol){
                return false;
            }
        return true;
    }

    bool isSymmetricMatrix(){
        auto A = *this;
        for (size_t i=0; i< shape[0]; i++)
            for (size_t j=0; j< shape[0]; j++)
                if(A[i][j] != A[j][i])return false;
        return shape[0] == shape[1];
    }


    size_t symmetricMatrixEigenVectorsAndValues(Matrix2D<T>& eigen_vectors, Matrix2D<T>& eigen_values)const
    {
        auto A = *this;
        if(!A.isSymmetricMatrix())
            return false;
        eigen_vectors = Matrix2D::eye(shape[0]);
        eigen_values = Matrix2D::zeros({shape[0], 1});


        if (shape[0] == 0)
            return false;
        if (shape[0] == 1)
        {
            eigen_values[0][0] = A[0][0];
            eigen_vectors[0][0] = 1;
            return true;
        }
        bool flag = true;
        size_t rot = 0, N = A.shape[0];
        for (size_t i=0; i< N; i++)
            eigen_values[i][0] = A[i][i];

        for(;flag;){
            flag = false;
            size_t p,q;
            for (p = 0; p< N; p++){
                for (q = p+1;q < N ; q++){
                    T app = eigen_values[p][0];
                    T aqq = eigen_values[q][0];
                    T apq = A[p][q];
                    T phi = ((T)0.5) * atan2((T)2*apq, aqq-app);
                    T c = cos(phi);
                    T s = sin(phi);
                    T app1 = pow(c,(T)2)*app-((T)2*s*c*apq)+(pow(s,(T)2)*aqq);
                    T aqq1 = pow(s,(T)2)*app+((T)2*s*c*apq)+(pow(c,(T)2)*aqq);
                    if( app1 !=  app || aqq1 != aqq ){
                        flag = true;
                        rot++;
                        eigen_values[p][0] = app1;
                        eigen_values[q][0] = aqq1;
                        A[p][q] = (T)0.0;
                        for (size_t i=0; i< p; i++){
                            T temp = A[i][p];
                            A[i][p] = c * temp - s * A[i][q];
                            A[i][q] = c * A[i][q] + s *temp;
                        }

                        for (size_t i=p+1; i< q; i++){
                            T temp = A[p][i];
                            A[p][i] = c * temp - s * A[i][q];
                            A[i][q] = c * A[i][q] + s * temp;
                        }

                        for (size_t i = q+1; i< N; i++){
                            T temp = A[p][i];
                            A[p][i] = c * temp - s * A[q][i];
                            A[q][i] = c * A[q][i] + s * temp;
                        }

                        for (size_t i=0; i< N; i++){
                            T temp = eigen_vectors[i][p];
                            eigen_vectors[i][p] = c * temp - s * eigen_vectors[i][q];
                            eigen_vectors[i][q] = c * eigen_vectors[i][q] + s * temp;
                        }
                    }

                }

            }

        }
        return rot;
    }


};


template <class T>
Matrix2D<T> operator* (const T& x, const  Matrix2D<T>& y)
{
    return y * x;
}


