#include "matrix2d.h"
#include "least_squares.h"
#include "datasets.h"
#include "lda.h"
#include "pca.h"

void unit_tests();

int main(int argc, char *argv[])
{

    {
        auto data = Matrix2D<long double>(Datasets<long double>::IRIS2Class2Feature::X());
        auto label = Matrix2D<size_t>(Datasets<long double>::IRIS2Class2Feature::Y());


        LDA<long double> lda1(data, label, Datasets<long double>::IRIS2Class2Feature::nb_class(), 2);

        auto tdata1 = lda1.transform(data);

        lda1.plotBoundw2d(data, label, {"Iris-setosa", "Iris-virginica"}, "LDA transform Data IRIS 2 classes");
    }

    {
        // PCA<long double> pca = PCA<long double>(data, 2);
        auto data = Matrix2D<long double>(Datasets<long double>::IRIS2Class::X());
        auto label = Matrix2D<size_t>(Datasets<long double>::IRIS2Class::Y());

        LDA<long double> lda(data, label, Datasets<long double>::IRIS2Class::nb_class(), 2);

        auto tdata = lda.transform(data);

        // lda.plotPoints2d(tdata, label, {"Iris-setosa", "Iris-virginica"}, "PCA Firt LDA transform");

        LDA<long double> lda1(tdata, label, Datasets<long double>::IRIS2Class::nb_class(), 2);

        auto tdata1 = lda1.transform(tdata);

        lda1.plotBoundw2d(tdata, label, {"Iris-setosa", "Iris-virginica"}, "LDA Dimension Reduction and LDA bonduary");
    }
    {

        auto data = Matrix2D<long double>(Datasets<long double>::IRIS2Class::X());
        auto label = Matrix2D<size_t>(Datasets<long double>::IRIS2Class::Y());

        PCA<long double> pca = PCA<long double>(data, 2);

        LDA<long double> lda(data, label, Datasets<long double>::IRIS2Class::nb_class(), 2);

        auto tdata = pca.transform(data);

        // lda.plotPoints2d(tdata, label, {"Iris-setosa", "Iris-virginica"}, "LDA transform");

        LDA<long double> lda1(tdata, label, Datasets<long double>::IRIS2Class::nb_class(), 2);

        auto tdata1 = lda1.transform(tdata);

        lda1.plotBoundw2d(tdata, label, {"Iris-setosa", "Iris-virginica"}, "PCA Dimension Reduction and LDA bonduary");
    }

    {
        auto data = Matrix2D<long double>(Datasets<long double>::IRIS::X());
        auto label = Matrix2D<size_t>(Datasets<long double>::IRIS::Y());

        LDA<long double> lda(data, label, Datasets<long double>::IRIS::nb_class(), 2);

        auto tdata = lda.transform(data);

        lda.plotPoints2d(tdata, label, {"Iris-setosa", "Iris-versicolor", "Iris-virginica"}, "LDA transform complete IRIS");

    }
    return 0;
}
