#pragma once

#include "matrix2d.h"
#include "least_squares.h"
#include "gnuplot.h"


#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>



template <class T>
class PCA{
private:
    PCA(){}
public:
    Matrix2D<T> mean;
    Matrix2D<T> featurek, centreded_data;



    PCA(const Matrix2D<T>& data,const size_t& N):mean({1,1}), featurek({1,1}), centreded_data({1,1}){
        auto D = data;
        mean = D.mean(0);

        centreded_data = data - mean;
        centreded_data.print_matrix();
        auto cT = centreded_data.transpose();
        auto COVm = cT.dot(centreded_data) * (1.0/static_cast<T>(D.shape[0] -1));

        Matrix2D<T> Evectors({1,1});
        Matrix2D<T> Evalues({1,1});

        COVm.symmetricMatrixEigenVectorsAndValues(Evectors, Evalues);

        featurek = topKFeatureVector(Evalues, Evectors, N);

        auto TT = transform(D);
//        D.print_matrix();
//        TT.print_matrix();
        auto ID = inverse_transform(TT);
//        ID.print_matrix();
        std::cout << D.almostClose(ID, 1e-7) << "\n";

    }


    void plotCompareLeastSquare( Matrix2D<T>& data, LeastSquares<T>& least_square, const std::string& title = ""){
        std::ofstream myfile;
        std::remove("/tmp/pca.dat");
        myfile.open ("/tmp/pca.dat");
        for(size_t i = 0; i < data.shape[0]; i++){
            for(size_t j = 0; j < data.shape[1]; j++)
                myfile << data[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();


        GnuplotPipe gp;

        std::stringstream ss;
        if( title.size() > 0)
            ss << "set title \"" << title << "\"; show title;" ;

        ss << "plot \"/tmp/pca.dat\" using 1:2 with points title \"" << "Original Data" <<  "\", ";
        ss << least_square.intercept_[least_square.mode] << " " << std::showpos << least_square.coef_[least_square.mode][0][0] << "*x  title \"LeastSquare\" ,";

        T intercetp = -featurek[0][1]/featurek[0][0]*mean[0][0] + mean[0][1];

        ss << " " << intercetp << " " << std::showpos << featurek[0][1]/featurek[0][0] << "*x title \"PCA Principal Component\"; ";




        gp.sendLine(ss.str());


    }


    void plotPoints( Matrix2D<T>& data1, Matrix2D<T>& data2, const std::string& title = ""){
        std::ofstream myfile;
        std::remove("/tmp/pca.dat");
        myfile.open ("/tmp/pca.dat");
        for(size_t i = 0; i < data1.shape[0]; i++){
            for(size_t j = 0; j < data1.shape[1]; j++)
                myfile << data1[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();

        myfile.open ("/tmp/pca1.dat");
        for(size_t i = 0; i < data2.shape[0]; i++){
            for(size_t j = 0; j < data2.shape[1]; j++)
                myfile << data2[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();

        GnuplotPipe gp;

        std::stringstream ss;
        if( title.size() > 0)
            ss << "set title \"" << title << "\"; show title;" ;

        ss << "plot \"/tmp/pca.dat\" using 1:2 with points title \"" << "Original Data" <<  "\", ";
        ss << " \"/tmp/pca1.dat\" using 1:2 with points title \"" << "Transformed Data" <<  "\"; ";




        gp.sendLine(ss.str());


    }

    void plotPoints3d( Matrix2D<T>& data1, Matrix2D<T>& data2, const std::string& title = ""){
        std::ofstream myfile;
        std::remove("/tmp/pca.dat");
        myfile.open ("/tmp/pca.dat");
        for(size_t i = 0; i < data1.shape[0]; i++){
            for(size_t j = 0; j < data1.shape[1]; j++)
                myfile << data1[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();
        std::remove("/tmp/pca1.dat");
        myfile.open ("/tmp/pca1.dat");
        for(size_t i = 0; i < data2.shape[0]; i++){
            for(size_t j = 0; j < data2.shape[1]; j++)
                myfile << data2[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();

        GnuplotPipe gp;

        std::stringstream ss;
        if( title.size() > 0)
            ss << "set title \"" << title << "\"; show title;" ;

        ss << "splot \"/tmp/pca.dat\" using 1:2:3 with points title \"" << "Original Data" <<  "\", ";
        ss << " \"/tmp/pca1.dat\" using 1:2:3 with points title \"" << "Transformed Data" <<  "\"; ";

        gp.sendLine(ss.str());
    }



    void plotPrincipalComponentMeanData(const std::string& title = ""){
        std::ofstream myfile;
        std::remove("/tmp/pca.dat");
        myfile.open ("/tmp/pca.dat");
        for(size_t i = 0; i < centreded_data.shape[0]; i++){
            for(size_t j = 0; j < centreded_data.shape[1]; j++)
                myfile << centreded_data[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();
        GnuplotPipe gp;

        std::stringstream ss;
        if( title.size() > 0)
            ss << "set title \"" << title << "\"; show title;" ;

        ss << "plot \"/tmp/pca.dat\" using 1:2 with points title \"" << "Mean Data" <<  "\", ";

        ss << " " << std::showpos << featurek[0][1]/featurek[0][0] << "*x title \"PCA Principal Component\"; ";




        gp.sendLine(ss.str());

    }

    void plotPrincipalComponentOriginalData(const std::string& title = ""){
        std::ofstream myfile;
        std::remove("/tmp/pca.dat");
        myfile.open ("/tmp/pca.dat");

        auto ori_data = centreded_data + mean;

        for(size_t i = 0; i < ori_data.shape[0]; i++){
            for(size_t j = 0; j < ori_data.shape[1]; j++)
                myfile << ori_data[i][j] << "\t";
            myfile <<  "\n";
        }
        myfile.close();
        GnuplotPipe gp;

        std::stringstream ss;

        T intercetp = -featurek[0][1]/featurek[0][0]*mean[0][0] + mean[0][1];
        if( title.size() > 0)
            ss << "set title \"" << title << "\"; show title;" ;
        ss << "plot \"/tmp/pca.dat\" using 1:2 with points title \"" << "Original Data" <<  "\", ";

        ss << " " << intercetp << " " << std::showpos << featurek[0][1]/featurek[0][0] << "*x title \"PCA Principal Component\"; ";

        gp.sendLine(ss.str());

    }

    Matrix2D<T> transform(Matrix2D<T>& data){
        auto ajust_dataT = (data - mean).transpose();
        auto featurekT = featurek.transpose();
        return featurekT.dot(ajust_dataT).transpose();
    }


    Matrix2D<T> inverse_transform(Matrix2D<T>& data){
        auto dataT = data.transpose();
        auto ajust_data = featurek.transpose().pseudo_inv().dot(dataT).transpose();
        return ajust_data + mean;
    }

    Matrix2D<T> topKFeatureVector(Matrix2D<T>& Evalues, Matrix2D<T>& Evectors, const size_t& N){

        std::vector<std::pair<T, size_t > > to_order;


        std::vector<Matrix2D<T> > vecs;
        for(size_t i = 0; i < Evalues.shape[0]; i++){

            Matrix2D<T> vec({Evectors.shape[0],1});
            for(size_t j = 0; j < Evectors.shape[0]; j++){
                vec[j][0] = Evectors[j][i];
            }


            to_order.push_back(std::make_pair(Evalues[i][0], i));
            vecs.push_back(vec);
        }
        // lamba function to order feature vector
        std::sort(to_order.begin(), to_order.end(),[](const std::pair<T, size_t > &a,
                                                                const std::pair<T, size_t > &b)
                                                  {
                                                      return (a.first > b.first);
                                                  });

        Matrix2D<T> featurek({Evectors.shape[0],N});

        for(size_t i = 0; i < N; i++){
            for(size_t j = 0; j < Evectors.shape[0]; j++){

                featurek[j][i] = vecs[to_order[i].second][j][0];
            }
        }

        return featurek;


    }

    virtual ~PCA(){

    }


};

//GnuplotPipe gp;
//gp.sendLine("plot  (1 2 3 4) (1 4 9 16)");
