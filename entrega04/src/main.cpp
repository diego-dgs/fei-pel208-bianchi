#include "matrix2d.h"
#include "least_squares.h"
#include "datasets.h"
#include "lda.h"
#include "pca.h"
#include "kmeans.h"

void unit_tests();

int main(int argc, char *argv[])
{
{
    // PCA<long double> pca = PCA<long double>(data, 2);
    auto data = Matrix2D<double>(Datasets<double>::PLANO_UNIFORM::X());
    auto km = kmeans<double>(7,1000, 1e-5, kmeans<double>::euclidean_distance ) ;
    km.fit(data);

    km.plotPoints2d("kmeans");
}
{
    auto data = Matrix2D<double>(Datasets<double>::IRIS::X());
    auto km = kmeans<double>(3,1000, 1e-5, kmeans<double>::euclidean_distance ) ;
    km.fit(data);
    km.plotPoints2d("Iris Kmeans result");

}
{
    auto data = Matrix2D<double>(Datasets<double>::seeds_dataset::X());
    auto km = kmeans<double>(3,1000, 1e-5, kmeans<double>::euclidean_distance ) ;
    km.fit(data);
    km.plotPoints2d("seeds-dataset Kmeans result");

}

{
    auto data = Matrix2D<double>(Datasets<double>::buddymove_holidayiq::X());
    auto km = kmeans<double>(3,1000, 1e-5, kmeans<double>::euclidean_distance ) ;
    km.fit(data);
    km.plotPoints2d("buddymove-holidayiq Kmeans result");

}

{
    auto data = Matrix2D<double>(Datasets<double>::PLANO_UNIFORM::X());
    auto km = kmeans<double>(3,1000, 1e-5, kmeans<double>::manhattan_distance ) ;
    km.fit(data);
    km.plotPoints2d("manhattan-distance Kmeans result");

}
    return 0;
}
