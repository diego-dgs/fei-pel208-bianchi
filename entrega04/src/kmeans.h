#pragma once

#include "matrix2d.h"
#include "least_squares.h"
#include "gnuplot.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <algorithm>
#include <math.h>
#include <stdexcept>
#include <map>
#include <vector>

#include "pca.h"

template <class T>
class kmeans
{
private:
    kmeans() {}
    size_t k, max_iter;
    T tolerance;
    Matrix2D<T> X;
    std::function<T(Matrix2D<T> &a, Matrix2D<T> &b)> distance;

    std::map<size_t, Matrix2D<T>> centroids;
    std::map<size_t, Matrix2D<T>> previous;
    std::map<size_t,  std::vector <size_t> > clusters;

public:
    static T euclidean_distance(Matrix2D<T> &a, Matrix2D<T> &b)
    {
        T ret = 0;

        auto A = a.reshape({a.shape[0] * a.shape[1], 1});
        auto B = b.reshape({b.shape[0] * b.shape[1], 1});
                
        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                ret += pow(A[i][0] - B[i][0], (T)2.0);
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        ret = std::sqrt(ret);
        return ret;
    }

    static T chebychev_distance(Matrix2D<T> &a, Matrix2D<T> &b)
    {
        T ret = 0;

        auto A = a.reshape({a.shape[0] * a.shape[1], 1});
        auto B = b.reshape({b.shape[0] * b.shape[1], 1});

        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                if(ret < abs(A[i][0] - B[i][0]))
                    ret = abs(A[i][0] - B[i][0]);
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        return ret;
    }

    static T cosine_similarity(Matrix2D<T> &a, Matrix2D<T> &b)
    {
        T ret = 0, ret1 = 0, ret2 = 0;

        auto A = a.reshape({a.shape[0] * a.shape[1], 1});
        auto B = b.reshape({b.shape[0] * b.shape[1], 1});
                
        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                ret += A[i][0] * B[i][0];
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        
        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                ret1 += pow(A[i][0], (T)2.0);
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        
        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                ret2 += pow(B[i][0], (T)2.0);
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        
        ret = std::sqrt(ret)/ (std::sqrt(ret1) * std::sqrt(ret2));
        return ret;
    }
    static T manhattan_distance(Matrix2D<T> &a, Matrix2D<T> &b)
    {
        T ret = 0;

        auto A = a.reshape({a.shape[0] * a.shape[1], 1});
        auto B = b.reshape({b.shape[0] * b.shape[1], 1});

        if (A.shape[0] == B.shape[0])
        {
            for (size_t i = 0; i < A.shape[0]; i++)
            {
                ret += abs(A[i][0] - B[i][0]);
            }
        }
        else
        {
            throw std::runtime_error(" a and b must be same dim");
        }
        return ret;
    }

    kmeans(const size_t &k, const size_t &max_iteration, const T &tolerance, std::function<T(Matrix2D<T> &a, Matrix2D<T> &b)> f)
    {
        this->k = k;
        this->max_iter = max_iteration;
        this->tolerance = tolerance;
        this->distance = f;
    }

    void fit(Matrix2D<T> &data)
    {
        X = data;
        for (size_t i = 0; i < this->k; i++)
        {

            auto rd = (size_t)rand() % X.shape[0];
            auto c  = Matrix2D<T>::zeros({1, X.shape[1]});
            for (size_t j = 0; j < c.shape[1]; j++){
                c[0][j] = X[rd][j];
            }
            this->centroids[i] = c;
        }


        for(size_t i = 0; i < this->max_iter; i++){
            this->clusters.clear();
            for (size_t j = 0; j < this->k; j++){
                this->clusters[j] =  std::vector< size_t >();
            }


            for (size_t j = 0; j < X.shape[0]; j++)
            {
                Matrix2D<T> c({1, X.shape[1]});
                for (size_t l = 0; l < c.shape[1]; l++){
                    c[0][l] = X[j][l];
                }
                
                size_t min_k = 0;
                T mim_v = (T)1e19;
                for (size_t jj = 0; jj< this->k; jj++){

                    T dist = this->distance(c, this->centroids[jj]);
                    if(dist < mim_v)
                    {
                        mim_v = dist;
                        min_k = jj;
                    }
                }
                this->clusters[min_k].push_back(j);

            }

            for (size_t jj = 0; jj< this->k; jj++)
            {
                this->previous[jj] = this->centroids[jj];
            }

            for (size_t jj = 0; jj< this->k; jj++)
            {
                auto mean = Matrix2D<T>::zeros(this->centroids[jj].shape);

                for(auto idx : this->clusters[jj])
                {
                    Matrix2D<T> c({1, X.shape[1]});
                    for (size_t l = 0; l < c.shape[1]; l++){
                        c[0][l] = X[idx][l];
                    }
                    for (size_t jjj = 0; jjj< c.shape[1]; jjj++)
                    {
                        mean[0][jjj] = mean[0][jjj] + c[0][jjj];
                    }
                }
                auto denom = (T)(1.0/(T)this->clusters[jj].size());
                for (size_t jjj = 0; jjj< mean.shape[1]; jjj++)
                {
                    mean[0][jjj] = mean[0][jjj] * denom;
                }
                
                // std::cout << this->clusters[jj].size() << "\n";
                this->centroids[jj] = mean;
                // this->centroids[jj].print_matrix();

            }
            // if(i % 5 == 0){
            //     this->plotPoints2d("kmeans iter " + std::to_string(i));
            // }
            if(i != 0)
            {
                bool equal = true;
                for (size_t jj = 0; jj< this->k; jj++)
                {
                    equal = equal && (this->previous[jj].almostClose(this->centroids[jj], this->tolerance));
                }
                if(equal)
                    break;
            }



        }
    }


    void plotPoints2d(const std::string &title = "")
    {
        if (X.shape[1] > 2){
            PCA<T> pca = PCA<T>(X, 2);
            X = pca.transform(X);
        }

        for (size_t j = 0; j < this->k; j++)
        {
            std::ofstream myfile;
            std::stringstream fname;
            fname << "/tmp/kmeans" << j << ".dat";
            std::remove(fname.str().c_str());
            myfile.open(fname.str().c_str());


            
            for (auto &idx: this->clusters[j])
            {
                    Matrix2D<T> data1({1, X.shape[1]});
                    for (size_t l = 0; l < data1.shape[1]; l++){
                        data1[0][l] = X[idx][l];
                    }
                    for (size_t j = 0; j < data1.shape[1]; j++)
                        myfile << data1[0][j] << "\t";
                    myfile << "\n";
                
            }
            
            myfile.close();
        }
        GnuplotPipe gp;

        std::stringstream ss;

        if (title.size() > 0)
            ss << "set title \"" << title << "\"; show title;";
        ss << "plot";
        for (size_t i = 0; i < this->k; i++)
        {
            std::stringstream fname;
            fname << "/tmp/kmeans" << i << ".dat";
            ss << " \"" << fname.str().c_str() << "\" using 1:2 with points ";
            
            ss << " title \"ID: " << i << "\"";
            
            if (i < (this->k - 1))
                ss << ", ";
        }
        std::cout << ss.str() << "\n";
        gp.sendLine(ss.str());
    }

    // void plotPoints3d(Matrix2D<T> &data1, Matrix2D<T> &data2, const std::string &title = "")
    // {
    //     std::ofstream myfile;
    //     std::remove("/tmp/kmeans.dat");
    //     myfile.open("/tmp/kmeans.dat");
    //     for (size_t i = 0; i < data1.shape[0]; i++)
    //     {
    //         for (size_t j = 0; j < data1.shape[1]; j++)
    //             myfile << data1[i][j] << "\t";
    //         myfile << "\n";
    //     }
    //     myfile.close();
    //     std::remove("/tmp/kmeans1.dat");
    //     myfile.open("/tmp/kmeans1.dat");
    //     for (size_t i = 0; i < data2.shape[0]; i++)
    //     {
    //         for (size_t j = 0; j < data2.shape[1]; j++)
    //             myfile << data2[i][j] << "\t";
    //         myfile << "\n";
    //     }
    //     myfile.close();

    //     GnuplotPipe gp;

    //     std::stringstream ss;
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";

    //     ss << "splot \"/tmp/kmeans.dat\" using 1:2:3 with points title \""
    //        << "Original Data"
    //        << "\", ";
    //     ss << " \"/tmp/kmeans1.dat\" using 1:2:3 with points title \""
    //        << "Transformed Data"
    //        << "\"; ";

    //     gp.sendLine(ss.str());
    // }

    // void plotPrincipalComponentMeanData(const std::string &title = "")
    // {
    //     std::ofstream myfile;
    //     std::remove("/tmp/kmeans.dat");
    //     myfile.open("/tmp/kmeans.dat");
    //     for (size_t i = 0; i < centreded_data.shape[0]; i++)
    //     {
    //         for (size_t j = 0; j < centreded_data.shape[1]; j++)
    //             myfile << centreded_data[i][j] << "\t";
    //         myfile << "\n";
    //     }
    //     myfile.close();
    //     GnuplotPipe gp;

    //     std::stringstream ss;
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";

    //     ss << "plot \"/tmp/kmeans.dat\" using 1:2 with points title \""
    //        << "Mean Data"
    //        << "\", ";

    //     ss << " " << std::showpos << featurek[0][1] / featurek[0][0] << "*x title \"kmeans Principal Component\"; ";

    //     gp.sendLine(ss.str());
    // }

    // void plotPrincipalComponentOriginalData(const std::string &title = "")
    // {
    //     std::ofstream myfile;
    //     std::remove("/tmp/kmeans.dat");
    //     myfile.open("/tmp/kmeans.dat");

    //     auto ori_data = centreded_data + mean;

    //     for (size_t i = 0; i < ori_data.shape[0]; i++)
    //     {
    //         for (size_t j = 0; j < ori_data.shape[1]; j++)
    //             myfile << ori_data[i][j] << "\t";
    //         myfile << "\n";
    //     }
    //     myfile.close();
    //     GnuplotPipe gp;

    //     std::stringstream ss;

    //     T intercetp = -featurek[0][1] / featurek[0][0] * mean[0][0] + mean[0][1];
    //     if (title.size() > 0)
    //         ss << "set title \"" << title << "\"; show title;";
    //     ss << "plot \"/tmp/kmeans.dat\" using 1:2 with points title \""
    //        << "Original Data"
    //        << "\", ";

    //     ss << " " << intercetp << " " << std::showpos << featurek[0][1] / featurek[0][0] << "*x title \"kmeans Principal Component\"; ";

    //     gp.sendLine(ss.str());
    // }

    // Matrix2D<T> transform(Matrix2D<T> &data)
    // {
    //     auto ajust_dataT = (data - mean).transpose();
    //     auto featurekT = featurek.transpose();
    //     return featurekT.dot(ajust_dataT).transpose();
    // }

    // Matrix2D<T> inverse_transform(Matrix2D<T> &data)
    // {
    //     auto dataT = data.transpose();
    //     auto ajust_data = featurek.transpose().pseudo_inv().dot(dataT).transpose();
    //     return ajust_data + mean;
    // }

    // Matrix2D<T> topKFeatureVector(Matrix2D<T> &Evalues, Matrix2D<T> &Evectors, const size_t &N)
    // {

    //     std::vector<std::pair<T, size_t>> to_order;

    //     std::vector<Matrix2D<T>> vecs;
    //     for (size_t i = 0; i < Evalues.shape[0]; i++)
    //     {

    //         Matrix2D<T> vec({Evectors.shape[0], 1});
    //         for (size_t j = 0; j < Evectors.shape[0]; j++)
    //         {
    //             vec[j][0] = Evectors[j][i];
    //         }

    //         to_order.push_back(std::make_pair(Evalues[i][0], i));
    //         vecs.push_back(vec);
    //     }
    //     // lamba function to order feature vector
    //     std::sort(to_order.begin(), to_order.end(), [](const std::pair<T, size_t> &a, const std::pair<T, size_t> &b) {
    //         return (a.first > b.first);
    //     });

    //     Matrix2D<T> featurek({Evectors.shape[0], N});

    //     for (size_t i = 0; i < N; i++)
    //     {
    //         for (size_t j = 0; j < Evectors.shape[0]; j++)
    //         {

    //             featurek[j][i] = vecs[to_order[i].second][j][0];
    //         }
    //     }

    //     return featurek;
    // }

    virtual ~kmeans()
    {
    }
};

//GnuplotPipe gp;
//gp.sendLine("plot  (1 2 3 4) (1 4 9 16)");
